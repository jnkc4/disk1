package com.example.gmh.proj;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;
import kr.jnkc.core.RecyclerAdapter;

import static kr.jnkc.tools.Define.POST;

public class List_Sort extends AppCompatActivity {
    // region 선언부
    String query;
    ArrayList sort_data = new ArrayList();
    Toolbar toolbar;
    RecyclerView recyclerview;
    Intent intent;
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();
    // endregion
    // region Callback 메서드
    CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int pid, String ServiceName, ArrayList result, ArrayList data, Object o) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("select_sort")) {
                    sort_data.clear();
                    sort_data.addAll(data);
                    if (data.get(0, 0) != null) {
                        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerview.setLayoutManager(layoutManager);
                        recyclerview.setAdapter(recyclerAdapter);
                    }
                }
            }
        }
    };
    // endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_sort);
        toolbar = (Toolbar) findViewById(R.id.listsort_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("분류 관리");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);
    }

    @Override
    protected void onResume() {
        super.onResume();
        recyclerview = (RecyclerView) findViewById(R.id.addsort_recy_list);
        query = "Select * From sort";
        dbinfor.query(query);
        commtask.execute("select_sort", POST, callback);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            case R.id.new_insert: {
                intent = new Intent(List_Sort.this, Add_Sort.class);
                startActivity(intent);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    RecyclerAdapter recyclerAdapter = new RecyclerAdapter(sort_data) {
        class ViewHolder extends kr.jnkc.core.ViewHolder {
            public TextView sort_namevalue;
            public Button config_item_button;

            public ViewHolder(View itemview) {
                super(itemview);
                sort_namevalue = (TextView) itemview.findViewById(R.id.addsort_namevalue_text);
                config_item_button = (Button) itemview.findViewById(R.id.addsort_config_button);
                config_item_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        intent = new Intent(List_Sort.this, Config_Sort.class);
                        intent.putExtra("name", sort_namevalue.getText());
                        startActivity(intent);
                    }
                });
            }
        }



        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewtype) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_sort_recyclerview, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            String Name = (String) getItem(position, "name");
            ViewHolder viewholder = (ViewHolder) holder;
            viewholder.sort_namevalue.setText(Name);
        }
    };
}
