package com.example.gmh.proj;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;
import kr.jnkc.core.RecyclerAdapter;

import static kr.jnkc.tools.Define.POST;

public class List_Buy extends AppCompatActivity {
    String query;
    String id;
    String dept;
    String level;
    ArrayList object_buy = new ArrayList();
    Bundle bundle;
    Toolbar toolbar;
    RecyclerView recyclerview;
    Intent intent = new Intent();
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();

    CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object object) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("select_buy")) {
                    object_buy.clear();
                    object_buy.addAll(data);
                    if (data.get(0, 0) != null) {
                        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerview.setLayoutManager(layoutManager);
                        recyclerview.setAdapter(recyclerAdapter);
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_buy);
        toolbar = (Toolbar) findViewById(R.id.listbuy_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("비품 구매 요청");
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back);
        intent = getIntent();
        bundle = intent.getExtras();
        if (bundle != null) {
            id = bundle.getString("ID");
            dept = bundle.getString("DEPT");
            level = bundle.getString("LEVEL");
        }
        recyclerview = (RecyclerView) findViewById(R.id.listbuy_list_recy);
        query = "select USER_DEPT, USER_ID, NAME, DATE_FORMAT(REG_DTTM,'%Y-%m-%d') as REG_DTTM, CASE STATE\n" +
                "WHEN '1' THEN '요청'\n" +
                "WHEN '2' THEN '접수'\n" +
                "WHEN '3' THEN '구매'\n" +
                "WHEN '4' THEN '완료'\n" +
                "WHEN '5' THEN '불가'\n" +
                "WHEN '6' THEN '취소'\n" +
                "end AS STATE From object_buy";
        dbinfor.query(query);
        commtask.execute("select_buy", POST, callback);
    }

    RecyclerAdapter recyclerAdapter = new RecyclerAdapter(object_buy) {
        class ViewHolder extends kr.jnkc.core.ViewHolder {
            public TextView user_dept;
            public TextView user_id;
            public TextView object_name;
            public TextView reg_dttm;
            public TextView object_state;
            public Button config_item_button;

            public ViewHolder(View itemview) {
                super(itemview);
                user_dept = (TextView) itemview.findViewById(R.id.listbuy_dept_value);
                user_id = (TextView) itemview.findViewById(R.id.listbuy_id_value);
                object_name = (TextView) itemview.findViewById(R.id.listbuy_name_value);
                reg_dttm = (TextView) itemview.findViewById(R.id.listbuy_reg_value);
                object_state = (TextView) itemview.findViewById(R.id.listbuy_state_value);
//                config_item_button = (Button) itemview.findViewById(R.id.addsort_config_button);
//                config_item_button.setOnClickListener(new View.OnClickListener() {
//                    @Override
//                    public void onClick(View v) {
//                        intent = new Intent(List_Buy.this, Config_Sort.class);
//                        intent.putExtra("name", sort_namevalue.getText());
//                        startActivity(intent);
//                    }
//                });
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewtype) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_buy_recyclerview, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            String dept_value = (String) getItem(position, "USER_DEPT");
            String id_value = (String) getItem(position, "USER_ID");
            String name_value = (String) getItem(position, "NAME");
            String reg_value = (String) getItem(position, "REG_DTTM");
            String state_value = (String) getItem(position, "STATE");

            ViewHolder viewholder = (ViewHolder) holder;
            viewholder.user_dept.setText(dept_value);
            viewholder.user_id.setText(id_value);
            viewholder.object_name.setText(name_value);
            viewholder.reg_dttm.setText(reg_value);
            viewholder.object_state.setText(state_value);
        }
    };

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            case R.id.new_insert: {
                intent = new Intent(List_Buy.this, Add_Buy.class);
                intent.putExtras(bundle);
                startActivity(intent);
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
