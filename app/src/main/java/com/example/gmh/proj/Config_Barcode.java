package com.example.gmh.proj;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;

import static kr.jnkc.tools.Define.POST;

public class Config_Barcode extends AppCompatActivity {
    String query;
    String barcode;
    Toolbar toolbar;
    TextView configcode_barcode;
    TextView configcode_itemname;
    TextView configcode_sort;
    TextView configcode_count;
    TextView configcode_deptuser;
    Button result_barcode;
    Intent intent = new Intent();
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();

    CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object o) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("select_barcode")) {
                    configcode_barcode.setText(data.get(0, 0).toString());
                    configcode_itemname.setText(data.get(0, 1).toString());
                    configcode_sort.setText(data.get(0, 2).toString());
                    configcode_count.setText(data.get(0, 3).toString());
                    configcode_deptuser.setText(data.get(0,4).toString() +" / "+ data.get(0,5));
                    configcode_barcode.setEnabled(false);
                    configcode_itemname.setEnabled(false);
                    configcode_sort.setEnabled(false);
                    configcode_count.setEnabled(false);
                    configcode_deptuser.setEnabled(false);
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config_barcode);
        toolbar = (Toolbar) findViewById(R.id.configcode_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("관리번호 상세정보");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);
        configcode_barcode = (TextView) findViewById(R.id.configcode_barcode_text);
        configcode_itemname = (TextView) findViewById(R.id.configcode_itemname_text);
        configcode_sort = (TextView) findViewById(R.id.configcode_sort_text);
        configcode_count = (TextView) findViewById(R.id.configcode_count_text);
        configcode_deptuser = (TextView) findViewById(R.id.configcode_deptuser_text);
        result_barcode = (Button) findViewById(R.id.configcode_barcode_button);
        intent = getIntent();
        barcode = intent.getStringExtra("BARCODE");

        query = "SELECT BARCODE, ITEM_NAME, SORT, COUNT, IFNULL(DEPT,'미 배정'), IFNULL(USER,'미 배정'), PRICE " +
                " FROM BARCODE " +
                " WHERE BARCODE = '" + barcode + "'";
        dbinfor.query(query);
        commtask.execute("select_barcode", POST, callback);

//        result_barcode.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                intent = new Intent(Config_Barcode.this, Result_Barcode.class);
//                intent.putExtra("WONO", barcode);
//                startActivity(intent);
//
//            }
//        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
            }
        }
        return super.onOptionsItemSelected(item);
    }
}

