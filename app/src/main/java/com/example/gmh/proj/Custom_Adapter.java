package com.example.gmh.proj;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import javax.xml.transform.Result;

import kr.jnkc.core.ArrayList;

public class Custom_Adapter extends BaseAdapter{

    Context context;
    ArrayList<Result_Item> datalist = new ArrayList<>();

    public Custom_Adapter(Context context, ArrayList<Result_Item> datalist){
        this.context = context;
        this.datalist = datalist;
    }

    @Override
    public int getCount() {
        return datalist.size();
    }

    @Override
    public Object getItem(int position) {
        return datalist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if(convertView == null){
            convertView = LayoutInflater.from(context).inflate(R.layout.config_listview,null);
        }
        TextView t1 = (TextView) convertView.findViewById(R.id.list_title);

        return convertView;
    }
}
