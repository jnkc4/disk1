package com.example.gmh.proj;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;

import static kr.jnkc.tools.Define.POST;

public class Popup_User_Info extends Activity {
    String query;
    TextView popup_id;
    TextView popup_name;
    TextView popup_dept;
    TextView popup_level;
    Button popup_close;
    Intent intent = new Intent();
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();

    CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object o) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("select_user")) {
                    popup_id.setText(data.get(0,0).toString());
                    popup_name.setText(data.get(0,1).toString());
                    popup_dept.setText(data.get(0,2).toString());
                    popup_level.setText(data.get(0,3).toString());
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_user_info);
        intent = getIntent();
        String ID = intent.getExtras().getString("ID");
        popup_close = (Button) findViewById(R.id.userpopup_close);
        popup_id = (TextView) findViewById(R.id.userpopup_id);
        popup_name = (TextView) findViewById(R.id.userpopup_name);
        popup_dept = (TextView) findViewById(R.id.userpopup_dept);
        popup_level = (TextView) findViewById(R.id.userpopup_level);

        query = "select ID, NAME ,DEPT,CASE LEVEL " +
                "WHEN '1' THEN '일반' " +
                "WHEN '2' THEN '비품관리자' " +
                "WHEN '3' THEN '부서장' " +
                "WHEN '4' THEN '시스템관리자' " +
                "end AS LEVEL FROM USER " +
                "WHERE ID = '" + ID + "'";
        dbinfor.query(query);
        commtask.execute("select_user", POST, callback);

        popup_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}