package com.example.gmh.proj;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;
import kr.jnkc.core.RecyclerAdapter;

import static kr.jnkc.tools.Define.POST;

public class List_User extends AppCompatActivity {
    // region 선언부
    String query;
    ArrayList user_data = new ArrayList();
    Toolbar toolbar;
    Spinner listuser_filter;
    RecyclerView recyclerview;
    Intent intent;
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();
    // endregion
    CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int pid, String ServiceName, ArrayList result, ArrayList data, Object o) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("select_user")) {
                    user_data.clear();
                    user_data.addAll(data);
                    if (data.get(0, 0) != null) {
                        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerview.setLayoutManager(layoutManager);
                        recyclerview.setAdapter(recyclerAdapter);
                    } else {
                        recyclerview.setAdapter(null);
                    }
                } else if (ServiceName.equals("select_dept")) {
                    data.add(0, "{name:전체보기}");
                    ArrayAdapter adapter = new ArrayAdapter(List_User.this, android.R.layout.simple_spinner_item, data.get("name"));
                    listuser_filter.setAdapter(adapter);
                    listuser_filter.setSelection(0);
                }
            }
        }
    };

    // region Create
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_user);
        toolbar = (Toolbar) findViewById(R.id.listuser_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("유저 조회");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);
        listuser_filter = (Spinner) findViewById(R.id.listuser_filter);
        query = "Select * From Dept";
        dbinfor.query(query);
        commtask.execute("select_dept", POST, callback);

    }

    // endregion
    // region Toolbar OptionsMenu Create & Click Event
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            case R.id.new_insert: {
                intent = new Intent(List_User.this, Add_User.class);
                startActivity(intent);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    // endregion
    // region Main
    @Override
    protected void onResume() {
        super.onResume();
        recyclerview = (RecyclerView) findViewById(R.id.listid_recy_list);
        listuser_filter.setSelection(0);
        listuser_filter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getSelectedItem().toString().equals("전체보기")) {
                    query = "select ID, NAME ,DEPT,CASE LEVEL\n" +
                            "WHEN '1' THEN '일반'\n" +
                            "WHEN '2' THEN '비품관리자'\n" +
                            "WHEN '3' THEN '부서장'\n" +
                            "WHEN '4' THEN '시스템관리자'\n" +
                            "end AS LEVEL FROM user " +
                            "Order BY LEVEL";
                    dbinfor.query(query);
                    commtask.execute("select_user", POST, callback);
                } else {
                    query = "select ID, NAME ,DEPT,CASE LEVEL " +
                            "WHEN '1' THEN '일반' " +
                            "WHEN '2' THEN '비품관리자' " +
                            "WHEN '3' THEN '부서장' " +
                            "WHEN '4' THEN '시스템관리자' " +
                            "end AS LEVEL from user " +
                            "where dept ='" + parent.getSelectedItem() + "'" +
                            "order by LEVEL";
                    dbinfor.query(query);
                    commtask.execute("select_user", POST, callback);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });
    }

    // endregion
    // region RecyclerAdapter
    RecyclerAdapter recyclerAdapter = new RecyclerAdapter(user_data) {
        class ViewHolder extends kr.jnkc.core.ViewHolder {
            public TextView id_text;
            public TextView name_text;
            public TextView dept_text;
            public TextView level_text;
            public Button user_config;

            public ViewHolder(View itemview) {
                super(itemview);
                id_text = (TextView) itemview.findViewById(R.id.listid_idvalue_text);
                name_text = (TextView) itemview.findViewById(R.id.listid_namevalue_text);
                dept_text = (TextView) itemview.findViewById(R.id.listid_deptvalue_text);
                level_text = (TextView) itemview.findViewById(R.id.listid_levelvalue_text);
                user_config = (Button) itemview.findViewById(R.id.listid_config_button);
                user_config.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        intent = new Intent(List_User.this, Config_User.class);
                        intent.putExtra("ID", id_text.getText());
                        startActivity(intent);
                    }
                });
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewtype) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_user_recyclerview, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            String ID = (String) getItem(position, "ID");
            String Name = (String) getItem(position, "NAME");
            String Dept = (String) getItem(position, "DEPT");
            String Level = (String) getItem(position, "LEVEL");
            ViewHolder viewholder = (ViewHolder) holder;
            viewholder.id_text.setText(ID);
            viewholder.id_text.setVisibility(recyclerview.GONE);
            viewholder.name_text.setText(Name);
            viewholder.dept_text.setText(Dept);
            viewholder.level_text.setText(Level);
        }
    };
    //endregion
}

