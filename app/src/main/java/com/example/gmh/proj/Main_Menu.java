package com.example.gmh.proj;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;

import static kr.jnkc.tools.Define.POST;

public class Main_Menu extends AppCompatActivity {
    // region 선언부
    String query;
    String id = null;
    String dept = null;
    String level = null;
    String name = null;
    Bundle bundle;
    Button main_button;
    Button dept_button;
    Button buy_button;
    Button config_button;
    Toolbar toolbar;
    ListView sidebar_list;
    DrawerLayout layout_draw;
    FrameLayout main_frame;
    Main_Menu_Main_Fragment main_frag;
    Main_Menu_Dept_Fragment dept_frag;
    Main_Menu_Buy_Fragment buy_frag;
    Main_Menu_Config_Fragment config_frag;
    FragmentTransaction tran;
    FragmentManager fm;
    Intent intent;


    // endregion
    // region BackPressed 버튼 클릭 이벤트
    @Override
    public void onBackPressed() {                       // 뒤로가기 버튼을 눌렀을 때 작동
        if (layout_draw.isDrawerOpen(sidebar_list)) {         // DrawerLayout이 Open 상태 일경우
            layout_draw.closeDrawer(sidebar_list);            // Close 해줌
        } else {
            super.onBackPressed();                      // 오픈되어 있지 않을 경우 이전 화면으로 이동
        }
    }

    // endregion
    // region Main
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main_menu);
        toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        sidebar_list = (ListView) findViewById(R.id.main_side_list);
        layout_draw = (DrawerLayout) findViewById(R.id.main_layout_draw);
        main_button = (Button) findViewById(R.id.main_main_button);
        dept_button = (Button) findViewById(R.id.main_dept_button);
        buy_button = (Button) findViewById(R.id.main_buy_button);
        config_button = (Button) findViewById(R.id.main_config_button);
        intent = getIntent();
        bundle = intent.getExtras();
        if (bundle != null) {
            id = bundle.getString("ID");
            dept = bundle.getString("DEPT");
            level = bundle.getString("LEVEL");
            name = bundle.getString("NAME");
        }
        setSupportActionBar(toolbar);
        final ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        String[] navItems = {dept + "\n" + id + "  ( " + level + " )", "비품 등록", "비품 현황 조회", "비품 구매 신청", "비품 통계", "비품 반납", "비품 분배", "비품 이동", "비품 폐기"};
        sidebar_list.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, navItems));
        sidebar_list.setOnItemClickListener(new DrawerItemClickListener());

        Button.OnClickListener TabClick = new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (v.getId()) {
                    case R.id.main_main_button: {
                        actionBar.setTitle(dept + "  " + name + " 님 환영합니다");
                        actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
                        setFrag(1);
                        break;
                    }
                    case R.id.main_dept_button: {
                        actionBar.setTitle(dept + " 부서 정보 조회");
                        setFrag(2);
                        break;
                    }
                    case R.id.main_buy_button: {
                        actionBar.setTitle(id + " 님의 구매 현황 조회");
                        setFrag(3);
                        break;
                    }
                    case R.id.main_config_button: {
                        actionBar.setTitle("설정");
                        setFrag(4);
                        break;
                    }
                }
            }
        };
        main_button.setOnClickListener(TabClick);
        dept_button.setOnClickListener(TabClick);
        buy_button.setOnClickListener(TabClick);
        config_button.setOnClickListener(TabClick);
        main_frag = new Main_Menu_Main_Fragment();
        dept_frag = new Main_Menu_Dept_Fragment();
        buy_frag = new Main_Menu_Buy_Fragment();
        config_frag = new Main_Menu_Config_Fragment();
        main_frag.setArguments(bundle);
        dept_frag.setArguments(bundle);
        buy_frag.setArguments(bundle);
        config_frag.setArguments(bundle);
        main_button.performClick();
    }

    // endregion
    // region Side_Bar Click Event
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
            switch (position) {
                case 0:
                    break;
                case 1:
                    intent = new Intent(Main_Menu.this, Add_Object.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    break;
                case 2:
                    intent = new Intent(Main_Menu.this, List_Object.class);
                    startActivity(intent);
                    break;
                case 3:
                    intent = new Intent(Main_Menu.this, List_Buy.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    break;
                case 4:
                    intent = new Intent(Main_Menu.this, List_Data.class);
                    intent.putExtra("dept", dept);
                    startActivity(intent);
                    break;
                case 5:
                    intent = new Intent(Main_Menu.this, Return_Object.class);
                    startActivity(intent);
                    break;
                case 6:
                    intent = new Intent(Main_Menu.this, Split_Object.class);
                    intent.putExtras(bundle);
                    startActivity(intent);
                    break;
                case 7:
                    intent = new Intent(Main_Menu.this, Move_Object.class);
                    startActivity(intent);
                    break;
                case 8:
                    intent = new Intent(Main_Menu.this, Destroy_Object.class);
                    startActivity(intent);
                    break;
            }
            layout_draw.closeDrawer(sidebar_list);
        }
    }

    // endregion
    // region Toolbar Click Event
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                layout_draw.openDrawer(sidebar_list);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    // endregion
    public void setFrag(int no) {
        fm = getFragmentManager();
        tran = fm.beginTransaction();
        switch (no) {
            case 1:
                tran.replace(R.id.main_frame, main_frag);
                tran.commit();
                break;
            case 2:
                tran.replace(R.id.main_frame, dept_frag);
                tran.commit();
                break;
            case 3:
                tran.replace(R.id.main_frame, buy_frag);
                tran.commit();
                break;
            case 4:
                tran.replace(R.id.main_frame, config_frag);
                tran.commit();
                break;
        }
    }

}
