package com.example.gmh.proj;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;


public class Admin_menu extends AppCompatActivity {
    // region 선언부
    Toolbar toolbar;
    ListView sidebar_list;
    DrawerLayout layout_draw;
    Intent intent;
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();
    // endregion
    // region Main
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.admin_menu);
        toolbar = (Toolbar) findViewById(R.id.admin_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("관리 페이지");
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back);
        String[] navItems = {"부서 관리", "유저 관리", "분류 관리", "관리번호 관리"};
        sidebar_list = (ListView) findViewById(R.id.admin_side_list);
        layout_draw = (DrawerLayout) findViewById(R.id.admin_layout_draw);
        sidebar_list.setAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, navItems));
        sidebar_list.setOnItemClickListener(new DrawerItemClickListener());
    }
    // endregion
    // region Side_Bar Click Event
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
            switch (position) {
                case 0:
                    intent = new Intent(Admin_menu.this, List_Dept.class);
                    startActivity(intent);
                    break;
                case 1:
                    intent = new Intent(Admin_menu.this, List_User.class);
                    startActivity(intent);
                    break;
                case 2:
                    intent = new Intent(Admin_menu.this, List_Sort.class);
                    startActivity(intent);
                    break;
                case 3:
                    intent = new Intent(Admin_menu.this, List_Barcode.class);
                    startActivity(intent);
                    break;
            }
            layout_draw.closeDrawer(sidebar_list);
        }
    }
    // endregion
    // region Toolbar OptionMenu Create & Click Event
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_admin, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            case R.id.side_menu: {
                layout_draw.openDrawer(sidebar_list);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
    // endregion
}
