package com.example.gmh.proj;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.google.zxing.qrcode.encoder.QRCode;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;
import kr.jnkc.core.RecyclerAdapter;

import static kr.jnkc.tools.Define.POST;

public class Result_Barcode extends AppCompatActivity {
    String query;
    ArrayList barcode_data = new ArrayList();
    RecyclerView recyclerview;
    Toolbar toolbar;
    Intent intent;
    DBInfor dbInfor = new DBInfor();
    CommTask commtask = new CommTask();

    CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int pid, String ServiceName, ArrayList result, ArrayList data, Object o) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("select_barcode")) {
                    barcode_data.clear();
                    barcode_data.addAll(data);
                    LinearLayoutManager layoutmanager = new LinearLayoutManager(getApplicationContext());
                    recyclerview.setLayoutManager(layoutmanager);
                    recyclerview.setAdapter(recyclerAdapter);
                }
            }
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.result_barcode);
        Login_Form.list_data.clear();
        toolbar = (Toolbar) findViewById(R.id.resultbarcode_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("유저 조회");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);
        recyclerview = (RecyclerView) findViewById(R.id.recy_barcode);
        intent = getIntent();
        String work_no = intent.getExtras().getString("WONO");
        query = "Select * From Barcode where Work_no = '" + work_no + "'";
        dbInfor.query(query);
        commtask.execute("select_barcode", POST, callback);
    }
    public void generateRQCode(String contents) {
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        try {
            Bitmap bitmap = toBitmap(qrCodeWriter.encode(contents, com.google.zxing.BarcodeFormat.QR_CODE, 100, 100));
        } catch (WriterException e) {
            e.printStackTrace();
        }
    }

    public static Bitmap toBitmap(BitMatrix matrix) {
        int height = matrix.getHeight();
        int width = matrix.getWidth();
        Bitmap bmp = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        for (int x = 0; x < width; x++) {
            for (int y = 0; y < height; y++) {
                bmp.setPixel(x, y, matrix.get(x, y) ? Color.BLACK : Color.WHITE);
            }
        }
        return bmp;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    RecyclerAdapter recyclerAdapter = new RecyclerAdapter(barcode_data) {
        class ViewHolder extends kr.jnkc.core.ViewHolder {
            public ImageView barcode_image;
            public TextView barcode;
            public ViewHolder(View itemview) {
                super(itemview);
                barcode_image = (ImageView) itemview.findViewById(R.id.barcode_image);
                barcode = (TextView) itemview.findViewById(R.id.result_barcode);
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewtype) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.result_barcode_recyclerview, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            String Barcode = (String) getItem(position, "BARCODE");
            ViewHolder viewholder = (ViewHolder) holder;
            QRCodeWriter qrCodeWriter = new QRCodeWriter();
            try {
                Bitmap bitmap = toBitmap(qrCodeWriter.encode(Barcode, com.google.zxing.BarcodeFormat.QR_CODE, 500, 500));
                viewholder.barcode_image.setImageBitmap(bitmap);
                viewholder.barcode.setText(Barcode);
            } catch (WriterException e) {
                e.printStackTrace();
            }
        }
    };
}
