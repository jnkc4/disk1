package com.example.gmh.proj;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;
import kr.jnkc.core.RecyclerAdapter;

import static kr.jnkc.tools.Define.POST;

public class List_Barcode extends AppCompatActivity {
    // region 선언부
    RecyclerView recyclerView;
    String query;
    ArrayList code_data = new ArrayList();
    Toolbar toolbar;
    Intent intent;
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();
    // endregion
    CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object object) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("select_code")) {
                    code_data.clear();
                    code_data.addAll(data);
                    if (data.get(0, 0) != null) {
                        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView.setLayoutManager(layoutManager);
                        recyclerView.setAdapter(recyclerAdapter);
                    }
                }
            }
        }
    };

    // region Create
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_barcode);
    }

    // endregion
    // region Main
    @Override
    protected void onResume() {
        super.onResume();
        toolbar = (Toolbar) findViewById(R.id.listobj_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("비품바코드 관리");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);
        recyclerView = (RecyclerView) findViewById(R.id.barcode_list);
        query = "Select * From barcode";
        dbinfor.query(query);
        commtask.execute("select_code", POST, callback);
    }

    // endregion
    // region Toolbar OptionMenu Create & Click Event
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            case R.id.new_insert: {
                intent = new Intent(List_Barcode.this, Add_Barcode.class);
                startActivity(intent);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    // endregion
    // region RecyclerAdapter
    RecyclerAdapter recyclerAdapter = new RecyclerAdapter(code_data) {
        class ViewHolder extends kr.jnkc.core.ViewHolder {
            public TextView listbarcode_code;
            public TextView listbarcode_itemname;
            public TextView listbarcode_sort;
            public TextView listbarcode_count;
            public Button barcode_config;

            public ViewHolder(View itemview) {
                super(itemview);
                listbarcode_code = (TextView) itemview.findViewById(R.id.listbarcode_code);
                listbarcode_itemname = (TextView) itemview.findViewById(R.id.listbarcode_itemname);
                listbarcode_sort = (TextView) itemview.findViewById(R.id.listbarcode_sort);
                listbarcode_count = (TextView) itemview.findViewById(R.id.listbarcode_count);
                barcode_config = (Button) itemview.findViewById(R.id.barcode_config_button);
                barcode_config.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        intent = new Intent(List_Barcode.this, Config_Barcode.class);
                        intent.putExtra("BARCODE", listbarcode_code.getText());
                        startActivity(intent);
                    }
                });
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewtype) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_barcode_recyclerview, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            String Wo = getItem(position, "BARCODE").toString();
            String Name = (String) getItem(position, "ITEM_NAME");
            String Sort = (String) getItem(position, "SORT");
            String Count = getItem(position, "COUNT").toString();
            ViewHolder viewholder = (ViewHolder) holder;
            viewholder.listbarcode_code.setText(Wo);
            viewholder.listbarcode_code.setVisibility(recyclerView.GONE); // WO_NO는 유저가 굳이 알 필요가 없으므로 보이지 않도록 설정
            viewholder.listbarcode_itemname.setText(Name);
            viewholder.listbarcode_sort.setText(Sort);
            viewholder.listbarcode_count.setText(Count);
        }
    };
    // endregion
}

