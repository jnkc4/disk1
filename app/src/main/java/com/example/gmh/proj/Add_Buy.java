package com.example.gmh.proj;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.webkit.WebView;
import android.widget.TextView;
import android.widget.Toast;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;

import static kr.jnkc.tools.Define.POST;

public class Add_Buy extends AppCompatActivity {
    String query;
    String id;
    String dept;
    String level;
    TextView user_dept;
    TextView user_id;
    TextView object_name;
    TextView object_price;
    TextView object_remark;
    TextView object_url;
    Bundle bundle;
    Toolbar toolbar;
    Intent intent;
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();

    CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object object) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("insert_buy")) {
                    Toast.makeText(Add_Buy.this, "정상 처리 되었습니다.", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_buy);
        toolbar = (Toolbar) findViewById(R.id.addbuy_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("구매 요청서 작성");
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back);
        user_dept = (TextView) findViewById(R.id.addbuy_dept_text);
        user_id = (TextView) findViewById(R.id.addbuy_user_text);
        object_name = (TextView) findViewById(R.id.addbuy_object_text);
        object_price = (TextView) findViewById(R.id.addbuy_price_text);
        object_remark = (TextView) findViewById(R.id.addbuy_remark_text);
        object_url = (TextView) findViewById(R.id.addbuy_url_text);
        intent = getIntent();
        bundle = intent.getExtras();
        if (bundle != null) {
            id = bundle.getString("ID");
            dept = bundle.getString("DEPT");
            level = bundle.getString("LEVEL");
        }
        user_dept.setText(dept);
        user_dept.setEnabled(false);
        user_id.setText(id);
        user_id.setEnabled(false);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_insert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Add_Buy.this);
        switch (item.getItemId()) {
            case android.R.id.home: {
                if (object_name.getText().toString().equals("") && object_price.getText().toString().equals("") && object_remark.getText().toString().equals("") && object_url.getText().toString().equals("")) {
                    finish();
                    return true;
                } else {
                    builder.setTitle("화면에서 나가시겠습니까?").setMessage("입력된 내용은 저장되지 않습니다.").setPositiveButton("취소", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).setNegativeButton("확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    builder.create();
                    builder.show();
                    return true;
                }
            }
            case R.id.data_insert: {
                query = "insert into object_buy(USER_DEPT, USER_ID, NAME, PRICE, REMARK, URL, STATE, REG_DTTM)\n" +
                        "values('" + user_dept.getText() + "','" + user_id.getText() + "','" + object_name.getText() + "','" + object_price.getText() + "','" + object_remark.getText() + "','" + object_url.getText() + "','1',(select TIMESTAMP(NOW())))";
                dbinfor.query(query);
                commtask.execute("insert_buy", POST, callback);
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
