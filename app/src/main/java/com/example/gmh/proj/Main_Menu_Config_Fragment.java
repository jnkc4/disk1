package com.example.gmh.proj;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.RecyclerAdapter;

public class Main_Menu_Config_Fragment extends Fragment {
    View view;
    ArrayList config_list = new ArrayList();

    RecyclerView recyclerview;
    Intent intent;
    String id;
    String level;
    String dept;
    Bundle bundle;

    public Main_Menu_Config_Fragment() {
    }

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {
        config_list.clear();
        view = inflater.inflate(R.layout.main_menu_config_fragment, container, false);
        id = getArguments().getString("ID");
        level = getArguments().getString("LEVEL");
        dept = getArguments().getString("DEPT");
        bundle = new Bundle();
        bundle.putString("ID", id);
        bundle.putString("DEPT", dept);
        bundle.putString("LEVEL", level);
        recyclerview = (RecyclerView) view.findViewById(R.id.confrag_admin_list);
        switch (level) {
            case "1":
                config_list.add("{name:부서관리, icon:1}");
                config_list.add("{name:유저관리, icon:2}");
                config_list.add("{name:분류관리, icon:3}");
                config_list.add("{name:비품관리, icon:4}");

                config_list.add("{name:비품등록, icon:4}");
                config_list.add("{name:비품정보조회, icon:4}");
                config_list.add("{name:비품조회, icon:4}");
                config_list.add("{name:비품분배, icon:4}");
                config_list.add("{name:비품반납, icon:4}");
                config_list.add("{name:비품폐기, icon:4}");

                config_list.add("{name:비밀번호변경, icon:4}");
                break;
            case "2":
                break;
            case "3":
                break;
            case "4":
                config_list.add("{name:부서관리, icon:1}");
                config_list.add("{name:유저관리, icon:2}");
                config_list.add("{name:분류관리, icon:3}");
                config_list.add("{name:비품관리, icon:4}");

                config_list.add("{name:비품등록, icon:4}");
                config_list.add("{name:비품정보조회, icon:4}");
                config_list.add("{name:비품조회, icon:4}");
                config_list.add("{name:비품분배, icon:4}");
                config_list.add("{name:비품반납, icon:4}");
                config_list.add("{name:비품폐기, icon:4}");

                config_list.add("{name:비밀번호변경, icon:4}");


                break;
        }
        LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext().getApplicationContext());
        recyclerview.addItemDecoration(new DividerItemDecoration(view.getContext().getApplicationContext(), layoutManager.getOrientation()));
        recyclerview.setLayoutManager(layoutManager);
        recyclerview.setAdapter(recyclerAdapter);
        return view;
    }

    RecyclerAdapter recyclerAdapter = new RecyclerAdapter(config_list) {
        class ViewHolder extends kr.jnkc.core.ViewHolder {
            public ImageView menu_icon;
            public TextView menu_name;

            public ViewHolder(View itemview) {
                super(itemview);
                menu_icon = (ImageView) itemview.findViewById(R.id.frag_icon);
                menu_name = (TextView) itemview.findViewById(R.id.frag_textview);
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewtype) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_menu_config_recyclerview, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            final String ID = (String) getItem(position, "name");
            ViewHolder viewholder = (ViewHolder) holder;
            viewholder.menu_name.setText(ID);
            switch ((int) getItem(position, "icon")) {
                case 1:
                    viewholder.menu_icon.setImageResource(R.drawable.baseline_lock_black_24dp);
                    break;
                case 2:
                    viewholder.menu_icon.setImageResource(R.drawable.baseline_lock_black_24dp);
                    break;
                case 3:
                    viewholder.menu_icon.setImageResource(R.drawable.baseline_lock_black_24dp);
                    break;
                case 4:
                    viewholder.menu_icon.setImageResource(R.drawable.baseline_lock_black_24dp);
            }
            viewholder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    switch (ID) {
                        case "부서관리":
                            intent = new Intent(v.getContext(), List_Dept.class);
                            v.getContext().startActivity(intent);
                            break;
                        case "유저관리":
                            intent = new Intent(v.getContext(), List_User.class);
                            v.getContext().startActivity(intent);
                            break;
                        case "분류관리":
                            intent = new Intent(v.getContext(), List_Sort.class);
                            v.getContext().startActivity(intent);
                            break;
                        case "비품관리":
                            intent = new Intent(v.getContext(), List_Barcode.class);
                            v.getContext().startActivity(intent);
                            break;
                        case "비품등록":
                            intent = new Intent(v.getContext(), Add_Object.class);
                            intent.putExtras(bundle);
                            v.getContext().startActivity(intent);
                            break;
                        case "비품조회":
                            intent = new Intent(v.getContext(), List_Object.class);
                            v.getContext().startActivity(intent);
                            break;
                        case "비품반납":
                            intent = new Intent(v.getContext(), Return_Object.class);
                            intent.putExtras(bundle);
                            v.getContext().startActivity(intent);
                            break;
                        case "비품분배":
                            intent = new Intent(v.getContext(), Split_Object.class);
                            intent.putExtras(bundle);
                            v.getContext().startActivity(intent);
                    }
                }
            });
        }
    };
}
