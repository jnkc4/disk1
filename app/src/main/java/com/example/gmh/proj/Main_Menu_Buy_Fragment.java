package com.example.gmh.proj;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Main_Menu_Buy_Fragment extends Fragment{
    View view;

    public Main_Menu_Buy_Fragment() {
    }

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {
        view = inflater.inflate(R.layout.main_menu_buy_fragment, container, false);
        return view;
    }

}
