package com.example.gmh.proj;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;
import kr.jnkc.core.RecyclerAdapter;

import static kr.jnkc.tools.Define.POST;

public class List_Object extends AppCompatActivity {
    String query;
    ArrayList object_list = new ArrayList();
    RecyclerView recyclerView;
    Toolbar toolbar;
    Intent intent = new Intent();
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();

    CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object o) {
            int code = (int) result.get(0,0);
            if(code == 200){
                if(ServiceName.equals("select_object")){
                    object_list.addAll(data);
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_object);
    }

    protected void onResume() {
        super.onResume();
        toolbar = (Toolbar) findViewById(R.id.listobj_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("비품 조회");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);
        recyclerView = (RecyclerView) findViewById(R.id.listobj_recy_view);

        query = "SELECT BARCODE ,SORT, ITEM_NAME, IFNULL(DEPT,'미 배정') AS DEPT, IFNULL(USER, '미 배정') AS USER FROM BARCODE";
        dbinfor.query(query);
        commtask.execute("select_object", POST, callback);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(recyclerAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    RecyclerAdapter recyclerAdapter = new RecyclerAdapter(object_list) {
        class ViewHolder extends kr.jnkc.core.ViewHolder {
            public TextView listobj_barcode_text;
            public TextView listobj_sort_text;
            public TextView listobj_name_text;
            public TextView listobj_dept_text;
            public TextView listobj_user_text;

            public ViewHolder(View itemview) {
                super(itemview);
                listobj_barcode_text = (TextView) itemview.findViewById(R.id.listobj_barcode_text);
                listobj_sort_text = (TextView) itemview.findViewById(R.id.listobj_sort_text);
                listobj_name_text = (TextView) itemview.findViewById(R.id.listobj_name_text);
                listobj_dept_text = (TextView) itemview.findViewById(R.id.listobj_dept_text);
                listobj_user_text = (TextView) itemview.findViewById(R.id.listobj_user_text);
                /*barcode_config.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        intent = new Intent(List_Object.this, Result_Barcode.class);
                        intent.putExtra("work_no", re_object_wo.getText());
                        startActivity(intent);
                    }
                });*/
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewtype) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_object_recyclerview, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            String Barcode = (String) getItem(position, "BARCODE");
            String Name = (String) getItem(position, "NAME");
            String Sort = (String) getItem(position, "SORT");
            String Dept = (String) getItem(position, "DEPT");
            String User = (String) getItem(position, "USER");
            ViewHolder viewholder = (ViewHolder) holder;
            viewholder.listobj_barcode_text.setText(Barcode);
            viewholder.listobj_sort_text.setText(Name);
            viewholder.listobj_name_text.setText(Sort);
            viewholder.listobj_dept_text.setText(Dept);
            viewholder.listobj_user_text.setText(User);
        }
    };
}
