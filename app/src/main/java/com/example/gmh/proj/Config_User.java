package com.example.gmh.proj;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;

import static kr.jnkc.tools.Define.POST;

public class Config_User extends AppCompatActivity {
    // region 선언부
    ArrayList temp_dept = new ArrayList();
    int dept_no;
    int level;
    int leader_check = 0;
    String query;
    Toolbar toolbar;
    Button Delete_button;
    TextView id_edit;
    TextView name_edit;
    TextView pw_edit;
    Spinner dept_edit;
    RadioGroup radiogroup;
    RadioButton level1;
    RadioButton level2;
    RadioButton level3;
    RadioButton level4;
    CommTask commtask = new CommTask();
    DBInfor dbinfor = new DBInfor();
    // endregion

    // region Callback 메서드
    final CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, final ArrayList data, Object object) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("dept_select")) {
                    ArrayAdapter adapter = new ArrayAdapter(Config_User.this, android.R.layout.simple_spinner_item, data.get("name"));
                    dept_edit.setAdapter(adapter);
                    temp_dept.clear();
                    temp_dept.addAll(data);
                    query = "Select ID, NAME ,DEPT,level,REG_DTTM From user where ID = '" + object.toString() + "'";
                    dbinfor.query(query);
                    commtask.execute("user_select", POST, callback);
                } else if (ServiceName.equals("user_select")) {
                    id_edit.setText(data.get(0, 0).toString());
                    name_edit.setText(data.get(0, 1).toString());
                    for (int i = 0; i < temp_dept.size(); i++) {
                        if (data.get(0, 2).equals(temp_dept.get(i, 0))) {
                            dept_no = i;
                            break;
                        }
                    }
                    dept_edit.setSelection(dept_no);
                    level = (int) data.get(0, 3);
                    switch (level) {
                        case 1: {
                            level1.setChecked(true);
                            break;
                        }
                        case 2: {
                            level2.setChecked(true);
                            break;
                        }
                        case 3: {
                            level3.setChecked(true);
                            break;
                        }
                        case 4: {
                            level4.setChecked(true);
                            break;
                        }
                    }
                } else if (ServiceName.equals("leader_check")) {
                    if (data.get(0, 0).toString().equals("null")) {
                        // 기존 부서에 부서장이 없는 경우
                        leader_check = leader_check + 1;
                        query = "Update user set DEPT = '" + dept_edit.getSelectedItem() + "', LEVEL='" + level + "' ,CON_DTTM = (SELECT TIMESTAMP(NOW())) where ID = '" + id_edit.getText() + "'";
                        dbinfor.query(query);
                        commtask.execute("update", POST, callback);
                    } else {
                        // 기존 부서에 부서장이 있는 경우
                        AlertDialog.Builder builder = new AlertDialog.Builder(Config_User.this);
                        builder.setTitle("부서장이 이미 존재합니다.").setMessage("부서장 등록 시 기존 부서장은 \n 일반 등급으로 강등됩니다.").setPositiveButton("취소", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).setNegativeButton("확인", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                leader_check = leader_check + 1;
                                query = "Update user set DEPT = '" + dept_edit.getSelectedItem() + "', LEVEL='" + level + "' ,CON_DTTM = (SELECT TIMESTAMP(NOW())) where ID = '" + id_edit.getText() + "'";
                                dbinfor.query(query);
                                commtask.execute("update", POST, callback, data.get(0,0));
                            }
                        });
                        builder.create();
                        builder.show();
                    }
//                    if (pw_edit.getText().toString().equals("")) {
//                        query = "Update user set PASSWORD ='" + pw_edit.getText() + "', DEPT = '" + dept_edit.getSelectedItem() + "', LEVEL='" + level + "', CON_DTTM = (SELECT TIMESTAMP(NOW())) where ID = '" + id_edit.getText() + "'";
//                    } else {
//                        query = "Update user set DEPT = '" + dept_edit.getSelectedItem() + "', LEVEL='" + level + "' ,CON_DTTM = (SELECT TIMESTAMP(NOW())) where ID = '" + id_edit.getText() + "'";
//                    }
                } else if (ServiceName.equals("update")) {
                    if (leader_check == 0) {
                        Toast.makeText(Config_User.this, "정상적으로 수정되었습니다.", Toast.LENGTH_SHORT).show();
                        finish();
                    } else {
                        leader_check = 0;
                        String leader_change = (String) object;
                        query = "update user set level = 1 where id = '"+leader_change+"'";
                        dbinfor.query(query);
                        commtask.execute("update",POST,callback);
                    }
                } else if (ServiceName.equals("delete")) {
                    Toast.makeText(Config_User.this, "정상 삭제 되었습니다.", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }
    };

    // endregion
    // region Main
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config_user);
        toolbar = (Toolbar) findViewById(R.id.configid_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("유저 정보 수정");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);
        System.setProperty("user.timezone", "Asia/Seoul");
        id_edit = (TextView) findViewById(R.id.configid_id_text);
        name_edit = (TextView) findViewById(R.id.configid_name_text);
        pw_edit = (TextView) findViewById(R.id.configid_pw_text);
        dept_edit = (Spinner) findViewById(R.id.configid_dept_sp);
        radiogroup = (RadioGroup) findViewById(R.id.configid_radiogroup);
        level1 = (RadioButton) findViewById(R.id.configid_radio_1);
        level2 = (RadioButton) findViewById(R.id.configid_radio_2);
        level3 = (RadioButton) findViewById(R.id.configid_radio_3);
        level4 = (RadioButton) findViewById(R.id.configid_radio_4);
        pw_edit.requestFocus();
        leader_check = 0;
        Intent intent = getIntent();
        String ID = intent.getExtras().getString("ID");
        query = "Select name From dept";
        dbinfor.query(query);
        commtask.execute("dept_select", POST, callback, ID);
        Delete_button = (Button) findViewById(R.id.configid_delete_button);
        Delete_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(Config_User.this);
                builder.setTitle("계정 삭제").setMessage("정말 삭제하시겠습니까?").setPositiveButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        query = "Delete From user where ID='" + id_edit.getText() + "'";
                        dbinfor.query(query);
                        commtask.execute("delete", POST, callback);
                    }
                }).setNegativeButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });
                builder.create();
                builder.show();
            }
        });

        RadioButton.OnClickListener radio_check_event = new RadioButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (level1.isChecked() == true) {
                    level = 1;
                } else if (level2.isChecked() == true) {
                    level = 2;
                } else if (level3.isChecked() == true) {
                    level = 3;
                } else if (level4.isChecked() == true) {
                    level = 4;
                } else {
                }
            }
        };
        level1.setOnClickListener(radio_check_event);
        level2.setOnClickListener(radio_check_event);
        level3.setOnClickListener(radio_check_event);
        level4.setOnClickListener(radio_check_event);
    }

    // endregion
    // region OptionMenu Create & Click Event
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_insert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Config_User.this);
        switch (item.getItemId()) {
            case android.R.id.home: {
                if (pw_edit.getText().toString().equals("") || pw_edit.getText().toString().equals("")) {
                    finish();
                    return true;
                } else {
                    builder.setTitle("화면에서 나가시겠습니까?").setMessage("입력된 내용은 저장되지 않습니다.").setPositiveButton("취소", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).setNegativeButton("확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    builder.create();
                    builder.show();
                    return true;
                }
            }
            case R.id.data_insert: {
                if (level == 3) {
                    query = "select * FROM user where dept='" + dept_edit.getSelectedItem() + "' and level = 3 ";
                    dbinfor.query(query);
                    commtask.execute("leader_check", POST, callback);
                    return true;
                } else {
                    query = "select * FROM user where dpet='" + dept_edit.getSelectedItem() + "' and level = 3 ";
                    dbinfor.query(query);
                    commtask.execute("leader_check", POST, callback);
                    return true;
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }
    // endregion
}
