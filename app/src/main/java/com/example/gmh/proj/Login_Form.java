package com.example.gmh.proj;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import kr.jnkc.core.AESInfor;
import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;

import static kr.jnkc.tools.Define.*;

public class Login_Form extends Activity {
    //region 선언부
    static ArrayList list_data = new ArrayList();
    ArrayList<Object> mainPropertys;
    TextView id;
    TextView pw;
    Button login;
//    AESInfor aesinfor = new AESInfor("http://192.168.0.139:3030", "", "PASSWORDKEYJNKCA", "ACKNJYEKDROWSSAP");  //  AES 암호화 접속 정보 저장
//    AESInfor aesinfor = new AESInfor("http://192.168.1.66:3030", "", "PASSWORDKEYJNKCA", "ACKNJYEKDROWSSAP");  //  AES 암호화 접속 정보 저장
    AESInfor aesinfor = new AESInfor("https://210.121.210.2:10902", "", "PASSWORDKEYJNKCA", "ACKNJYEKDROWSSAP");  //
//        static DBInfor dbinfor = new DBInfor("http://192.168.0.139:3030", "mes", "tbe#2520", "106.249.236.106", "1433", "BK"); // MSSQL 데이터베이스 접속 정보 저장
    Intent intent;
//    static DBInfor dbinfor = new DBInfor("http://192.168.0.139:3030", "root", "P@ssw0rd", "127.0.0.1", "3306", "android_proj"); // MariaDB (MySQL) 데이터베이스 접속 정보 저장
//    static DBInfor dbinfor = new DBInfor("http://192.168.1.66:3030", "root", "P@ssw0rd", "192.168.0.139", "3306", "android_proj"); // MariaDB (MySQL) 데이터베이스 접속 정보 저장
    static DBInfor dbinfor = new DBInfor("https://210.121.210.2:10902", "root", "P@ssw0rd", "192.168.0.101", "3308", "android_proj"); //
    static CommTask commtask = new CommTask();
    //endregion
    //region Callback 메소드
    static final CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object object) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("AES")) {
                    ArrayList rs = data.get("RESULT");
                    ArrayList pp = (ArrayList) object;
                    String id = pp.get(2).toString();
                    String pw = (String) rs.get(0);
                    String query = "Select count(*) from user where ID='" + id + "' AND PASSWORD='" + pw + "'";
                    dbinfor.query(query);
                    commtask.execute("loginCK", POST, callback, object);
                } else if (ServiceName.equals("loginCK")) {
                    ArrayList pp = (ArrayList) object;
                    Context context = (Context) pp.get(1);
                    String id = pp.get(2).toString();
                    if ((int) data.get(0, 0) == 1) {
                        String query = "Select ID, DEPT, LEVEL,NAME from user where ID='" + id + "'";
                        dbinfor.query(query);
                        commtask.execute("login", POST, callback, object);
                    } else {
                        Toast.makeText(context, "ID 혹은 비밀번호를 확인하여 주십시오.", Toast.LENGTH_SHORT).show();
                    }
                } else if (ServiceName.equals("login")) {
                    ArrayList pp = (ArrayList) object;
                    Intent intent = (Intent) pp.get(0);
                    Context context = (Context) pp.get(1);
                    Bundle bundle = new Bundle();
                    bundle.putString("ID", data.get(0, 0).toString());
                    bundle.putString("DEPT", data.get(0, 1).toString());
                    bundle.putString("LEVEL", data.get(0, 2).toString());
                    bundle.putString("NAME",data.get(0,3).toString());
                    intent.putExtras(bundle);
                    context.startActivity(intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)); // 서비스는 태스크가 없기 때문에 액티비티를 시작하려면 NEW_TASK_FALG가 필요
                } else if (ServiceName.equals("user_select")) {
                    if (data.get(0, 0) != null) {
                        list_data.clear();
                        list_data.addAll(data);
                    }
                } else if (ServiceName.equals("user_select_value")) {
                    if (data.get(0, 0) != null) {
                        list_data.clear();
                        list_data.addAll(data);
                    }
                } else if (ServiceName.equals("code_list_select")) {
                    if (data.get(0, 0) != null) {
                        list_data.clear();
                        list_data.addAll(data);
                    }
                } else if (ServiceName.equals("barcode_select")) {
                    if (data.get(0, 0) != null) {
                        list_data.clear();
                        list_data.addAll(data);
                    }
                } else if (ServiceName.equals("object_select")) {
                    if (data.get(0, 0) != null) {
                        list_data.clear();
                        list_data.addAll(data);
                    }
                } else if (ServiceName.equals("dept_select")) {
                    if (data.get(0, 0) != null) {
                        list_data.clear();
                        list_data.addAll(data);
                    }
                } else if (ServiceName.equals("sort_select")) {
                    if (data.get(0, 0) != null) {
                        list_data.clear();
                        list_data.addAll(data);
                    }
                }
            } else {
                ArrayList pp = (ArrayList) object;
                Context context = (Context) pp.get(1);
                Toast.makeText(context, "Error Code : "+code + "\nError Message : " + result.get(0,2).toString() + "\n에러가 발생하였습니다. \n관리자에게 문의하세요", Toast.LENGTH_SHORT).show();
            }
        }
    };
    //endregion
    // region Main
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_form);
        id = (TextView) findViewById(R.id.login_id_text);
        pw = (TextView) findViewById(R.id.login_pw_text);
        login = (Button) findViewById(R.id.login_login_button);
        dbinfor.stream(true);
        dbinfor.DBMS(MYSQL);                                                        //DataBase 타입을 MYSQL로 설정

        intent = new Intent(this.getApplicationContext(), Main_Menu.class);
        mainPropertys = new ArrayList<>();
        mainPropertys.add(intent);
        mainPropertys.add(this.getApplicationContext());
        mainPropertys.add(id.getText());

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(id.getText().toString().equals("") || pw.getText().toString().equals("")){
                    Toast.makeText(Login_Form.this, "ID 혹은 비밀번호를 입력하여 주십시오.", Toast.LENGTH_SHORT).show();
                }else {
                    aesinfor.data(pw.getText().toString());
                    commtask.encryption("AES", POST, callback, mainPropertys);
                }
            }
        });
    }
    //endregion
}
