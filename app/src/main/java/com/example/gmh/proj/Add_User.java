package com.example.gmh.proj;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import kr.jnkc.core.AESInfor;
import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;

import static kr.jnkc.tools.Define.POST;

public class Add_User extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    // region 선언부
    ArrayList over = new ArrayList();
    String query;
    String dept;
    int check = 0;
    int leader_change;
    int level;
    Toolbar toolbar;
    TextView id_add;
    TextView pw_add;
    TextView name_add;
    Spinner dept_add;
    RadioGroup radiogroup;
    RadioButton level1;
    RadioButton level2;
    RadioButton level3;
    RadioButton level4;
    Button wndqhr;
    AESInfor aesinfor = new AESInfor();
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();
    // endregion
    // region Callback 메소드
    final CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, final ArrayList data, Object object) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("select_dept")) {
                    ArrayAdapter adapter = new ArrayAdapter(Add_User.this, android.R.layout.simple_spinner_item, data.get("name"));
                    dept_add.setAdapter(adapter);
                } else if (ServiceName.equals("wndqhr")) {
                    if (data.get("ID").size() > 0) {
                        Toast.makeText(Add_User.this, "이미 존재하는 아이디입니다.", Toast.LENGTH_SHORT).show();
                    } else if (id_add.length() <= 0) {
                        Toast.makeText(Add_User.this, "아이디를 입력하여 주십시오.", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(Add_User.this, "사용할 수 있는 아이디입니다.", Toast.LENGTH_SHORT).show();
//                        id_add.setEnabled(false);
                        id_add.setEnabled(false);
                        wndqhr.setText("ID 변경");
                        check = check + 1;
                    }
                } else if (ServiceName.equals("leader_check")) {
                    if (data.get(0, 0).toString().equals("null")) {
                        aesinfor.data(pw_add.getText().toString());
                        commtask.encryption("AES", POST, callback, null);
                        query = "Update dept Set leader='" + id_add.getText().toString() + "' where name ='" + dept_add.getSelectedItem() + "'";
                        dbinfor.query(query);
                        commtask.execute("add_user", POST, callback);
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(Add_User.this);
                        builder.setTitle("부서장이 이미 존재합니다.").setMessage("부서장 등록 시 기존 부서장은 \n 일반 등급으로 강등됩니다.").setPositiveButton("취소", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                            }
                        }).setNegativeButton("확인", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                leader_change = leader_change + 1;
                                aesinfor.data(pw_add.getText().toString());
                                commtask.encryption("AES", POST, callback, null);
                                query = "update user set level=1 where id='"+ data.get(0,0) + "'";
                                dbinfor.query(query);
                                commtask.execute("add_user", POST, callback);
                            }
                        });
                        builder.create();
                        builder.show();
                    }
                } else if (ServiceName.equals("AES")) {
                    over = data.get("RESULT");
                    query = "INSERT INTO USER(ID,NAME,PASSWORD,DEPT,LEVEL,REG_DTTM) VALUES('" + id_add.getText() + "','" + name_add.getText() + "','" + over.get(0) + "','" + dept + "','" + level + "',(select TIMESTAMP(NOW())))";
                    dbinfor.query(query);
                    if (leader_change == 0) {
                        commtask.execute("add_user", POST, callback);
                    } else {
                        commtask.execute("leader_change", POST, callback);
                    }
                } else if (ServiceName.equals("add_user")) {
                    Toast.makeText(Add_User.this, "정상적으로 추가되었습니다.", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }
    };

    // endregion
    // region Main
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_user);
        toolbar = (Toolbar) findViewById(R.id.adduser_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("계정 생성");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);
        id_add = (TextView) findViewById(R.id.adduser_id_text);
        pw_add = (TextView) findViewById(R.id.adduser_pw_text);
        name_add = (TextView) findViewById(R.id.adduser_name_text);
        dept_add = (Spinner) findViewById(R.id.adduser_dept_sp);
        dept_add.setOnItemSelectedListener(Add_User.this);
        radiogroup = (RadioGroup) findViewById(R.id.adduser_radiogroup);
        level1 = (RadioButton) findViewById(R.id.adduser_radio_1);
        level2 = (RadioButton) findViewById(R.id.adduser_radio_2);
        level3 = (RadioButton) findViewById(R.id.adduser_radio_3);
        level4 = (RadioButton) findViewById(R.id.adduser_radio_4);
        wndqhr = (Button) findViewById(R.id.adduser_wndqhr_button);
        level = 1;
        leader_change = 0;
        query = "Select * From dept";
        dbinfor.query(query);
        commtask.execute("select_dept", POST, callback);
        wndqhr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check == 0) {
                    query = "Select * From user where ID='" + id_add.getText() + "'";
                    dbinfor.query(query);
                    commtask.execute("wndqhr", POST, callback);
                } else {
                    id_add.setEnabled(true);
                    id_add.requestFocus();
                    wndqhr.setText("중복 확인");
                    check = check - 1;
                }
            }
        });
        RadioButton.OnClickListener radio_check_event = new RadioButton.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (level1.isChecked() == true) {
                    level = 1;
                } else if (level2.isChecked() == true) {
                    level = 2;
                } else if (level3.isChecked() == true) {
                    level = 3;
                } else if (level4.isChecked() == true) {
                    level = 4;
                } else {
                }
            }
        };
        level1.setChecked(true);
        level1.setOnClickListener(radio_check_event);
        level2.setOnClickListener(radio_check_event);
        level3.setOnClickListener(radio_check_event);
        level4.setOnClickListener(radio_check_event);
    }

    // endregion
    // region Toolbar OptionMenu Create & Click Event
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_insert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Add_User.this);
        switch (item.getItemId()) {
            case android.R.id.home: {
                if (id_add.getText().toString().equals("") && pw_add.getText().toString().equals("")) {
                    finish();
                    return true;
                } else {
                    builder.setTitle("화면에서 나가시겠습니까?").setMessage("입력된 내용은 저장되지 않습니다.").setPositiveButton("취소", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).setNegativeButton("확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    builder.create();
                    builder.show();
                    return true;
                }
            }
            case R.id.data_insert: {
                if (check == 0) {
                    Toast.makeText(Add_User.this, "ID 중복 확인을 먼저 진행하여 주십시오.", Toast.LENGTH_SHORT).show();
                } else if (pw_add.length() < 8) {
                    Toast.makeText(Add_User.this, "비밀번호는 8자 이상입니다.", Toast.LENGTH_SHORT).show();
                    pw_add.requestFocus();
                } else if (name_add.getText().toString().equals("")) {
                    Toast.makeText(this, "이름을 입력하여 주십시오.", Toast.LENGTH_SHORT).show();
                } else if (level == 3) {
                    query = "SELECT * FROM user WHERE dept='" + dept_add.getSelectedItem() + "' and level=3";
                    dbinfor.query(query);
                    commtask.execute("leader_check", POST, callback);
                } else {
                    aesinfor.data(pw_add.getText().toString());
                    commtask.encryption("AES", POST, callback, null);
                }
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    // endregion
    // region Spinner ItemSelect Event
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        dept = parent.getSelectedItem().toString();
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
    // endregion
}
