package com.example.gmh.proj;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;
import kr.jnkc.core.RecyclerAdapter;

import static kr.jnkc.tools.Define.POST;

public class Main_Menu_Dept_Fragment extends Fragment {
    View view;
    String dept;
    String query;
    String type;
    ArrayList data_list = new ArrayList();
    Spinner deptfrag_dept_spinner;
    TextView deptfrag_name;
    TextView deptfrag_leader;
    TextView deptfrag_count;
    RadioGroup deptfrag_radiogroup;
    RadioButton deptfrag_radio1;
    RadioButton deptfrag_radio2;
    RecyclerView deptfrag_recy;
    Intent intent;
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();

    CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object o) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("select_dept")) {
                    ArrayAdapter adapter = new ArrayAdapter(view.getContext().getApplicationContext(), android.R.layout.simple_spinner_item, data.get("name"));
                    deptfrag_dept_spinner.setAdapter(adapter);
                    for (int i = 0; i < data.size(); i++) {
                        if (dept.equals(data.get(i, 0).toString())) {
                            deptfrag_dept_spinner.setSelection(i);
                            break;
                        }
                    }
                } else if (ServiceName.equals("select_dept_info")) {
                    deptfrag_name.setText(data.get(0, 0).toString());
                    deptfrag_leader.setText(data.get(0, 1).toString());
                    deptfrag_count.setText(data.get(0, 2).toString());
                } else if (ServiceName.equals("check_type")) {
                    data_list.clear();
                    data_list.addAll(data);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext().getApplicationContext());
                    deptfrag_recy.addItemDecoration(new DividerItemDecoration(view.getContext().getApplicationContext(), layoutManager.getOrientation()));
                    deptfrag_recy.setLayoutManager(layoutManager);
                    deptfrag_recy.setAdapter(recyclerAdapter);
                } else if (ServiceName.equals("select_type")) {

                }
            }
        }
    };

    public Main_Menu_Dept_Fragment() {
    }

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {
        view = inflater.inflate(R.layout.main_menu_dept_fragment, container, false);
        deptfrag_name = (TextView) view.findViewById(R.id.deptfrag_name);
        deptfrag_leader = (TextView) view.findViewById(R.id.deptfrag_leader);
        deptfrag_count = (TextView) view.findViewById(R.id.deptfrag_count);
        deptfrag_radiogroup = (RadioGroup) view.findViewById(R.id.deptfrag_radiogroup);
        deptfrag_radio1 = (RadioButton) view.findViewById(R.id.deptfrag_radio1);
        deptfrag_radio2 = (RadioButton) view.findViewById(R.id.deptfrag_radio2);
        deptfrag_dept_spinner = (Spinner) view.findViewById(R.id.deptfrag_dept_spinner);
        deptfrag_recy = (RecyclerView) view.findViewById(R.id.deptfrag_recy);
        dept = getArguments().getString("DEPT");
        query = "Select name From dept";
        dbinfor.query(query);
        commtask.execute("select_dept", POST, callback, dept);
        deptfrag_radio1.setOnCheckedChangeListener(radio_event);
        deptfrag_radio2.setOnCheckedChangeListener(radio_event);

        deptfrag_dept_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                query = "Select D.name, IFNULL((select NAME FROM USER where dept = D.name and level=3),'공석') AS leader ,IFNULL((select count(ID) FROM USER WHERE dept = D.name),0) as count " +
                        "From dept AS D " +
                        "where name = '" + deptfrag_dept_spinner.getSelectedItem() + "'";
                dbinfor.query(query);
                commtask.execute("select_dept_info", POST, callback);
                deptfrag_radio1.setChecked(true);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return view;
    }

    RadioButton.OnCheckedChangeListener radio_event = new RadioButton.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            if (deptfrag_radio1.isChecked() == true) {
                query = "SELECT ID ,NAME AS VAL1, CASE LEVEL " +
                        "WHEN '1' THEN '일반' " +
                        "WHEN '2' THEN '비품관리자' " +
                        "WHEN '3' THEN '부서장' " +
                        "WHEN '4' THEN '시스템관리자' " +
                        "END AS VAL2 FROM USER WHERE DEPT = '" + deptfrag_dept_spinner.getSelectedItem() + "' " +
                        "ORDER BY LEVEL DESC";
                dbinfor.query(query);
                commtask.execute("check_type", POST, callback);
            } else if (deptfrag_radio2.isChecked() == true) {
                query = "SELECT BARCODE, SORT AS VAL1, ITEM_NAME AS VAL2, (SELECT NAME FROM USER WHERE ID=B.USER) AS VAL3 " +
                        "FROM BARCODE AS B " +
                        "WHERE DEPT = '" + deptfrag_dept_spinner.getSelectedItem() + "'";
                dbinfor.query(query);
                commtask.execute("check_type", POST, callback);
            }
        }
    };

    RecyclerAdapter recyclerAdapter = new RecyclerAdapter(data_list) {
        class ViewHolder extends kr.jnkc.core.ViewHolder {
            public TextView val1_text;
            public TextView val2_text;
            public TextView val3_text;

            public ViewHolder(View itemview) {
                super(itemview);
                val1_text = (TextView) itemview.findViewById(R.id.deptfrag_recy_val1);
                val2_text = (TextView) itemview.findViewById(R.id.deptfrag_recy_val2);
                val3_text = (TextView) itemview.findViewById(R.id.deptfrag_recy_val3);
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewtype) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_menu_dept_recyclerview, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            String VAL1 = (String) getItem(position, "VAL1");
            String VAL2 = (String) getItem(position, "VAL2");

            ViewHolder viewholder = (ViewHolder) holder;
            viewholder.val1_text.setText(VAL1);
            viewholder.val2_text.setText(VAL2);
            if (deptfrag_radio1.isChecked() == true) {
                viewholder.val3_text.setVisibility(deptfrag_recy.GONE);
                viewholder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        intent = new Intent(view.getContext(), Popup_User_Info.class);
                        intent.putExtra("ID", getItem(position, "ID").toString());
                        view.getContext().startActivity(intent);
                    }
                });
            } else {
                String VAL3 = (String) getItem(position, "VAL3");
                viewholder.val3_text.setText(VAL3);
                viewholder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        intent = new Intent(view.getContext(), Popup_User_Object.class);
                        intent.putExtra("BARCODE", getItem(position, "BARCODE").toString());
                        intent.putExtra("TYPE", "DEPT");
                        view.getContext().startActivity(intent);
                    }
                });
            }

        }
    };

}
