package com.example.gmh.proj;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;
import kr.jnkc.core.RecyclerAdapter;

import static kr.jnkc.tools.Define.POST;

public class Add_Sort extends AppCompatActivity {
    Toolbar toolbar;
    TextView name;
    TextView remark;
    CommTask commtask = new CommTask();
    DBInfor dbinfor = new DBInfor();
    String query;
    final CommTask.Callback sort_callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object object) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("sort_select")) {
                    if ((int) data.get(0, 0) == 0) {
                        query = "insert into sort(name,remark) values('" + name.getText().toString() + "', '" + remark.getText().toString() + "')";
                        dbinfor.query(query);
                        commtask.execute("sort_insert", POST, sort_callback);
                    } else {
                        Toast.makeText(Add_Sort.this, "이미 존재하는 분류명입니다.", Toast.LENGTH_SHORT).show();
                    }
                } else if (ServiceName.equals("sort_insert")) {
                    Toast.makeText(Add_Sort.this, "분류가 정상적으로 추가 되었습니다.", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_sort);
        toolbar = (Toolbar) findViewById(R.id.addsort_toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("분류 등록");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);
        name = (TextView) findViewById(R.id.addsort_name_edit);
        remark = (TextView) findViewById(R.id.addsort_remark_edit);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_insert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Add_Sort.this);
        switch (item.getItemId()) {
            case android.R.id.home: {
                if (name.getText().toString().equals("") && remark.getText().toString().equals("")) {
                    finish();
                    return true;
                } else {
                    builder.setTitle("화면에서 나가시겠습니까?").setMessage("입력된 내용은 저장되지 않습니다.").setPositiveButton("취소", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).setNegativeButton("확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    builder.create();
                    builder.show();
                    return true;
                }
            }
            case R.id.data_insert: {
                if (name.getText().toString().equals("")) {
                    Toast.makeText(this, "분류명을 입력하십시오.", Toast.LENGTH_SHORT).show();
                    return true;
                } else if (remark.getText().toString().equals("")) {
                    Toast.makeText(this, "분류 정보를 입력하십시오.", Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    query = "select count(*) from sort where name='" + name.getText() + "'";
                    dbinfor.query(query);
                    commtask.execute("sort_select", POST, sort_callback);
                    return true;
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
