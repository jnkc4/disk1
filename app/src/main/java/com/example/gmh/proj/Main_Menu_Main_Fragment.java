package com.example.gmh.proj;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;
import kr.jnkc.core.RecyclerAdapter;

import static kr.jnkc.tools.Define.POST;

public class Main_Menu_Main_Fragment extends Fragment {
    String query;
    String id;
    ArrayList user_object_list = new ArrayList();
    Bundle bundle;
    View view;
    Intent intent;
    TextView mainfrag_name;
    TextView mainfrag_dept;
    TextView mainfrag_level;
    TextView mainfrag_login;
    RecyclerView mainfrag_recy;
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();

    CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object o) {
            int code = (int) result.get(0, 0);
            if (ServiceName.equals("select_user")) {
                mainfrag_name.setText(data.get(0, 0) + "   (" + data.get(0, 1) + ")");
                mainfrag_dept.setText(data.get(0, 2).toString());
                mainfrag_level.setText(data.get(0, 3).toString());
                query = "select BARCODE, SORT, ITEM_NAME from BARCODE where user ='" + id + "'";
                dbinfor.query(query);
                commtask.execute("select_object", POST, callback);
            } else if (ServiceName.equals("select_object")) {
                user_object_list.clear();
                user_object_list.addAll(data);
                LinearLayoutManager layoutManager = new LinearLayoutManager(view.getContext().getApplicationContext());
                mainfrag_recy.addItemDecoration(new DividerItemDecoration(view.getContext().getApplicationContext(), layoutManager.getOrientation()));
                mainfrag_recy.setLayoutManager(layoutManager);
                mainfrag_recy.setAdapter(recyclerAdapter);
            }
        }
    };

    public Main_Menu_Main_Fragment() {
    }

    @Override
    public void onCreate(Bundle saveInstanceState) {
        super.onCreate(saveInstanceState);

    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstance) {
        view = inflater.inflate(R.layout.main_menu_main_fragment, container, false);
        mainfrag_name = (TextView) view.findViewById(R.id.mainfrag_name);
        mainfrag_dept = (TextView) view.findViewById(R.id.mainfrag_dept);
        mainfrag_level = (TextView) view.findViewById(R.id.mainfrag_level);
        mainfrag_login = (TextView) view.findViewById(R.id.mainfrag_login);
        mainfrag_login = (TextView) view.findViewById(R.id.mainfrag_object_count);
        mainfrag_recy = (RecyclerView) view.findViewById(R.id.mainfrag_recy_list);
        id = getArguments().getString("ID");
        bundle = getArguments();
        query = "Select NAME, ID, DEPT, CASE LEVEL " +
                "WHEN '1' THEN '일반' " +
                "WHEN '2' THEN '비품관리자' " +
                "WHEN '3' THEN '부서장' " +
                "WHEN '4' THEN '시스템관리자' " +
                "END AS LEVEL " +
                "From USER where ID='" + id + "'";
        dbinfor.query(query);
        commtask.execute("select_user", POST, callback);

        return view;
    }

    RecyclerAdapter recyclerAdapter = new RecyclerAdapter(user_object_list) {
        class ViewHolder extends kr.jnkc.core.ViewHolder {
            public TextView sort_text;
            public TextView name_text;

            public ViewHolder(View itemview) {
                super(itemview);
                sort_text = (TextView) itemview.findViewById(R.id.mainfrag_recy_sort);
                name_text = (TextView) itemview.findViewById(R.id.mainfrag_recy_name);
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewtype) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_menu_main_fragment_recyclerview, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
            String SORT = (String) getItem(position, "SORT");
            String NAME = (String) getItem(position, "ITEM_NAME");
            ViewHolder viewholder = (ViewHolder) holder;
            viewholder.sort_text.setText(SORT);
            viewholder.name_text.setText(NAME);
            viewholder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    intent = new Intent(view.getContext(), Popup_User_Object.class);
                    intent.putExtra("BARCODE", getItem(position, "BARCODE").toString());
                    intent.putExtra("TYPE", "main");
                    view.getContext().startActivity(intent);
                }
            });
        }
    };
}
