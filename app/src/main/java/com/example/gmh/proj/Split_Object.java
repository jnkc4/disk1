package com.example.gmh.proj;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;

import static kr.jnkc.tools.Define.POST;

public class Split_Object extends AppCompatActivity {
    String query;
    Bundle bundle;
    String id;
    String dept;
    String level;
    Toolbar toolbar;
    TextView split_barcode;
    TextView split_sort;
    TextView split_name;
    TextView split_count;
    TextView split_out_count;
    TextView split_remark;
    Spinner split_from_dept;
    Spinner split_from_user;
    Button split_scan_button;
    IntentIntegrator integrator = new IntentIntegrator(Split_Object.this);
    Intent intent;
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();

    CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object o) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("select_dept")) {
                    ArrayAdapter adapter = new ArrayAdapter(Split_Object.this, android.R.layout.simple_spinner_dropdown_item, data.get("name"));
                    split_from_dept.setAdapter(adapter);
                } else if (ServiceName.equals("select_user")) {
                    ArrayAdapter adapter = new ArrayAdapter(Split_Object.this, android.R.layout.simple_spinner_dropdown_item, data.get("ID"));
                    split_from_user.setAdapter(adapter);
                } else if (ServiceName.equals("select_barcode")) {
                    if (data.get(0, 0) == null) {
                        Toast.makeText(Split_Object.this, "존재하지 않는 바코드 입니다.", Toast.LENGTH_SHORT).show();
                    } else {
                        if (data.get(0, 2).toString().equals("0")) {
                            Toast.makeText(Split_Object.this, "남은 수량이 없습니다.", Toast.LENGTH_SHORT).show();
                            split_sort.setText("");
                            split_name.setText("");
                            split_count.setText("");
                        } else if (dept == data.get(0,3).toString()) {
                            Toast.makeText(Split_Object.this, "다른 부서의 비품을 분배할 수 없습니다.", Toast.LENGTH_SHORT).show();
                        } else {
                            split_sort.setText(data.get(0, 0).toString());
                            split_name.setText(data.get(0, 1).toString());
                            split_count.setText(data.get(0, 2).toString());
                        }
                    }
                } else if (ServiceName.equals("match_count")) {
                    if (data.get(0, 0).toString().equals(split_count.getText().toString())) {
                        query = "Update BARCODE SET COUNT = COUNT - " + split_out_count.getText() + " where BARCODE ='" + split_barcode + "'";
                        dbinfor.query(query);
                        commtask.execute("update_work", POST, callback);
                    } else {
                        Toast.makeText(Split_Object.this, "현재 바코드의 수량이 변경되었습니다.\n 새로고침하여 주십시오.", Toast.LENGTH_SHORT).show();
                    }
                } else if (ServiceName.equals("update_work")) {
                    String remark;
                    if (split_remark.getText().toString().equals("")) {
                        remark = dept + "의 " + id + " 님께서 " + split_from_dept.getSelectedItem() + "의 " + split_from_user.getSelectedItem() + "님에게 분배하였습니다.";
                    } else {
                        remark = split_remark.getText().toString();
                    }
                    query = "insert into split_object(BARCODE, ITEM_NAME,TO_DEPT,TO_USER, FROM_DEPT, FROM_USER, OUT_COUNT, ,S_ID,REG_DTTM, remark) " +
                            "VALUES('" + split_barcode.getText() + "', '" + split_name.getText() + "', " + "'" + dept + "', '" + id + "', '" + split_from_dept.getSelectedItem() + "', '" + split_from_user.getSelectedItem() + "'," +
                            "'" + split_out_count.getText() + "', (select TIMESTAMP(NOW())), '" + remark + "')";
                    dbinfor.query(query);
                    commtask.execute("insert_select_split", POST, callback);
                } else if (ServiceName.equals("insert_select_split")) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(Split_Object.this);
                    builder = new AlertDialog.Builder(Split_Object.this);
                    builder.setTitle("분배가 완료되었습니다.").setMessage("추가로 분배 하시겠습니까?").setPositiveButton("아니오", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    }).setNegativeButton("네", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            split_scan_button.performClick();
                            split_out_count.setText("");
                            split_remark.setText("");
                        }
                    });
                    builder.create();
                    builder.show();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.split_object);
        toolbar = (Toolbar) findViewById(R.id.split_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("비품 분배");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);
        split_barcode = (TextView) findViewById(R.id.split_barcode_text);
        split_sort = (TextView) findViewById(R.id.split_sort_text);
        split_name = (TextView) findViewById(R.id.split_name_text);
        split_count = (TextView) findViewById(R.id.split_count_text);
        split_out_count = (TextView) findViewById(R.id.split_out_count_text);
        split_scan_button = (Button) findViewById(R.id.split_scan_button);
        split_remark = (TextView) findViewById(R.id.split_remark_text);
        split_from_dept = (Spinner) findViewById(R.id.split_fromdept_sp);
        split_from_user = (Spinner) findViewById(R.id.split_fromuser_sp);
        integrator.initiateScan();
        intent = getIntent();
        bundle = intent.getExtras();
        if (bundle != null) {
            id = bundle.getString("ID");
            dept = bundle.getString("DEPT");
            level = bundle.getString("LEVEL");
        }
        query = "SELECT * FROM DEPT";
        dbinfor.query(query);
        commtask.execute("select_dept", POST, callback);

        split_scan_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                integrator.initiateScan();
            }
        });

        split_from_dept.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                query = "select * from user where dept ='" + parent.getSelectedItem().toString() + "'";
                dbinfor.query(query);
                commtask.execute("select_user", POST, callback);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IntentIntegrator.REQUEST_CODE) {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            split_barcode.setText(result.getContents());
            if (result.getContents() == null) {
                Toast.makeText(this, "바코드 읽기 취소", Toast.LENGTH_LONG).show();
            } else if (split_barcode.getText().toString().substring(0, 2).equals("OE")) {
                Toast.makeText(this, "개별 바코드는 분배할 수 없습니다.", Toast.LENGTH_SHORT).show();
            } else {
                query = "SELECT SORT, ITEM_NAME, COUNT,DEPT FROM BARCODE A WHERE BARCODE='" + split_barcode.getText() + "'";
                dbinfor.query(query);
                commtask.execute("select_barcode", POST, callback);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_insert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Split_Object.this);
        switch (item.getItemId()) {
            case android.R.id.home: {
                if (split_barcode.getText().toString().equals("") && split_out_count.getText().toString().equals("")) {
                    finish();
                    return true;
                } else {
                    builder.setTitle("화면에서 나가시겠습니까?").setMessage("입력된 내용은 저장되지 않습니다.").setPositiveButton("취소", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).setNegativeButton("확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    builder.create();
                    builder.show();
                    return true;
                }
            }
            case R.id.data_insert: {
                if (split_out_count.getText().toString().equals("")) {
                    Toast.makeText(Split_Object.this, "분배 수량을 입력하여 주십시오.", Toast.LENGTH_SHORT).show();
                } else if (Integer.parseInt(split_count.getText().toString()) < Integer.parseInt(split_out_count.getText().toString())) {
                    Toast.makeText(Split_Object.this, "현재 수량 보다 많이 분배 할 수 없습니다.", Toast.LENGTH_SHORT).show();
                } else {
                    query = "Select COUNT " +
                            "FROM BARCODE  " +
                            "WHERE BARCODE = '" + split_barcode.getText() + "'";
                    dbinfor.query(query);
                    commtask.execute("match_count", POST, callback);
                }
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
