package com.example.gmh.proj;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import org.w3c.dom.Text;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;

import static kr.jnkc.tools.Define.POST;

public class Return_Object extends AppCompatActivity {
    String id;
    String dept;
    String level;
    String query;
    Bundle bundle;
    Intent intent;
    Toolbar toolbar;
    TextView return_barcode;
    TextView return_name;
    TextView return_sort;
    TextView return_reason;
    TextView return_dept;
    TextView return_user;
    Button return_scan;
    IntentIntegrator integrator = new IntentIntegrator(Return_Object.this);
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();

    CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object o) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("return_search")) {
                    if (data.get(0, 0) == null) {
                        Toast.makeText(Return_Object.this, "존재하지 않는 바코드 입니다.", Toast.LENGTH_SHORT).show();
                        return;
                    } else if (data.get(0, 2).toString().equals("null")) {
                        Toast.makeText(Return_Object.this, "등록되지 않은 비품은 반납할 수 없습니다.", Toast.LENGTH_SHORT).show();

                    } else if (level == "1") {
                        Toast.makeText(Return_Object.this, "일반 등급은 반납이 불가능 합니다.", Toast.LENGTH_SHORT).show();
                    } else if (dept != data.get(0,3).toString()) {
                        Toast.makeText(Return_Object.this, "다른 부서의 비품을 대신 반납할 수 없습니다.", Toast.LENGTH_SHORT).show();
                    } else {
                        return_name.setText(data.get(0, 0).toString());
                        return_sort.setText(data.get(0, 1).toString());
                        return_user.setText(data.get(0, 2).toString());
                        return_dept.setText(data.get(0, 3).toString());
                        return_reason.setText(data.get(0, 2).toString() + " 님 비품 반납");
                    }
                } else if (ServiceName.equals("update_barcode")) {
                    query = "insert into return_object(BARCODE, SORT, ITEM_NAME, DEPT, USER, B_ID, REG_DTTM) " +
                            "values('" + return_barcode.getText() + "','" + return_sort.getText() + "','" + return_name.getText() + "','" + return_dept.getText() + "','" + return_user.getText() + "' " +
                            ", '" + id + "', (select TIMESTAMP(NOW())))";
                    dbinfor.query(query);
                    commtask.execute("insert_return", POST, callback);
                } else if (ServiceName.equals("insert_return")) {
                    Toast.makeText(Return_Object.this, "정상 반납 되었습니다.", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.return_object);
        toolbar = (Toolbar) findViewById(R.id.return_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("비품 반납");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);
        return_barcode = (TextView) findViewById(R.id.return_barcode_text);
        return_name = (TextView) findViewById(R.id.return_object_text);
        return_sort = (TextView) findViewById(R.id.return_sort_text);
        return_reason = (TextView) findViewById(R.id.return_remark_text);
        return_dept = (TextView) findViewById(R.id.return_dept_text);
        return_user = (TextView) findViewById(R.id.return_user_text);
        return_scan = (Button) findViewById(R.id.retunr_scan_button);
        integrator.initiateScan();
        intent = getIntent();
        bundle = intent.getExtras();
        if (bundle != null) {
            id = bundle.getString("ID");
            dept = bundle.getString("DEPT");
            level = bundle.getString("LEVEL");
        }
        return_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                integrator.initiateScan();
            }
        });

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IntentIntegrator.REQUEST_CODE) {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (result.getContents() == null) {
                Toast.makeText(this, "바코드 읽기 취소", Toast.LENGTH_LONG).show();
            } else {
                return_barcode.setText(result.getContents());
                query = "SELECT ITEM_NAME, SORT, (Select NAME FROM USER WHERE ID = A.USER), DEPT FROM BARCODE A WHERE BARCODE='" + return_barcode.getText() + "'";
                dbinfor.query(query);
                commtask.execute("return_search", POST, callback);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_insert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Return_Object.this);
        switch (item.getItemId()) {
            case android.R.id.home: {
                if (return_barcode.getText().toString().equals("") && return_reason.getText().toString().equals("")) {
                    finish();
                    return true;
                } else {
                    builder.setTitle("화면에서 나가시겠습니까?").setMessage("입력된 내용은 저장되지 않습니다.").setPositiveButton("취소", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).setNegativeButton("확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    builder.create();
                    builder.show();
                    return true;
                }

            }
            case R.id.data_insert: {
                if (return_barcode.getText().toString().equals("")) {
                    Toast.makeText(this, "Barcode를 입력하세요", Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    query = "update barcode set dept=null, user=null where barcode = '" + return_barcode.getText() + "'";
                    dbinfor.query(query);
                    commtask.execute("update_barcode", POST, callback);
                }
            }

        }
        return super.onOptionsItemSelected(item);
    }
}
