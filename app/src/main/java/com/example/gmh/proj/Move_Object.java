package com.example.gmh.proj;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;

import static kr.jnkc.tools.Define.POST;

public class Move_Object extends AppCompatActivity {
    String query;
    String remark;
    String sort;
    Toolbar toolbar;
    TextView moveobj_barcode;
    TextView moveobj_name;
    TextView moveobj_dept;
    TextView moveobj_user;
    Spinner moveobj_todept;
    Spinner moveobj_touser;
    TextView moveobj_remark;
    Button moveobj_scan;
    IntentIntegrator integrator = new IntentIntegrator(Move_Object.this);

    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();

    CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object o) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("select_barcode")) {
                    if (data.get(0, 0) == null) {
                        Toast.makeText(Move_Object.this, "존재하지 않는 바코드 입니다.", Toast.LENGTH_SHORT).show();
                        moveobj_name.setText("");
                        moveobj_dept.setText("");
                        moveobj_user.setText("");
                    } else if (data.get(0, 2).toString().equals("null")) {
                        Toast.makeText(Move_Object.this, "사용자가 등록되지 않은 바코드입니다.", Toast.LENGTH_SHORT).show();
                        moveobj_name.setText("");
                        moveobj_dept.setText("");
                        moveobj_user.setText("");
                    } else {
                        moveobj_name.setText(data.get(0, 1).toString());
                        moveobj_dept.setText(data.get(0, 2).toString());
                        moveobj_user.setText(data.get(0, 3).toString());
                        sort = data.get(0, 4).toString();
                        query = "Select * FROM DEPT";
                        dbinfor.query(query);
                        commtask.execute("select_dept", POST, callback);
                    }
                } else if (ServiceName.equals("select_dept")) {
                    ArrayAdapter adapter = new ArrayAdapter(Move_Object.this, android.R.layout.simple_spinner_dropdown_item, data.get("name"));
                    moveobj_todept.setAdapter(adapter);

                } else if (ServiceName.equals("select_user")) {
                    ArrayAdapter adapter = new ArrayAdapter(Move_Object.this, android.R.layout.simple_spinner_dropdown_item, data.get("ID"));
                    moveobj_touser.setAdapter(adapter);
                } else if (ServiceName.equals("update_barcode")) {
                    if (moveobj_remark.getText().toString().equals("")) {
                        remark = moveobj_dept.getText() + " 부서의 " + moveobj_user.getText() + " 님에서 " + moveobj_todept.getSelectedItem() + " 부서의 " + moveobj_touser.getSelectedItem() + " 님으로 이동";
                    }
                    query = "insert into move_barcode(barcode, sort, ITEM_NAME, FROM_DEPT, FROM_USER, TO_DEPT, TO_USER, REMARK) " +
                            "Values('" + moveobj_barcode.getText() + "', '" + sort + "', '" + moveobj_name.getText() + "', '" + moveobj_dept.getText() + "', '" + moveobj_user.getText() + "', " +
                            "'" + moveobj_todept.getSelectedItem() + "', '" + moveobj_touser.getSelectedItem() + "', '" + remark + "')";
                    dbinfor.query(query);
                    commtask.execute("insert_move", POST, callback);
                } else if (ServiceName.equals("insert_move")) {
                    Toast.makeText(Move_Object.this, "이동이 완료 되었습니다.", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.move_object);
        toolbar = (Toolbar) findViewById(R.id.moveobj_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle("비품 이동");
        actionBar.setHomeAsUpIndicator(R.drawable.ic_back);
        moveobj_barcode = (TextView) findViewById(R.id.moveobj_barcode_text);
        moveobj_name = (TextView) findViewById(R.id.moveobj_name_text);
        moveobj_dept = (TextView) findViewById(R.id.moveobj_dept_text);
        moveobj_user = (TextView) findViewById(R.id.moveobj_user_text);
        moveobj_todept = (Spinner) findViewById(R.id.moveobj_todept_sp);
        moveobj_touser = (Spinner) findViewById(R.id.moveobj_touser_sp);
        moveobj_remark = (TextView) findViewById(R.id.moveobj_remark_text);
        moveobj_scan = (Button) findViewById(R.id.moveobj_scan_button);
        moveobj_barcode.setEnabled(false);
        moveobj_name.setEnabled(false);
        moveobj_dept.setEnabled(false);
        moveobj_user.setEnabled(false);
        integrator.initiateScan();

        moveobj_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                integrator.initiateScan();
            }
        });

        moveobj_todept.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                query = "SELECT * FROM USER WHERE DEPT = '" + moveobj_todept.getSelectedItem() + "'";
                dbinfor.query(query);
                commtask.execute("select_user", POST, callback);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_insert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Move_Object.this);
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            case R.id.data_insert: {
                if (moveobj_dept.getText().toString().equals(moveobj_todept.getSelectedItem().toString()) && moveobj_user.getText().toString().equals(moveobj_touser.getSelectedItem().toString())) {
                    Toast.makeText(this, "동일한 유저에게 이동할 수 없습니다.", Toast.LENGTH_SHORT).show();
                } else {
                    query = "UPDATE BARCODE SET DEPT='" + moveobj_todept.getSelectedItem() + "', USER='" + moveobj_touser.getSelectedItem() + "' WHERE BARCODE='" + moveobj_barcode.getText() + "'";
                    dbinfor.query(query);
                    commtask.execute("update_barcode", POST, callback);
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == IntentIntegrator.REQUEST_CODE) {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (result.getContents() == null) {
                Toast.makeText(this, "바코드 읽기 취소", Toast.LENGTH_LONG).show();
            } else {
                moveobj_barcode.setText(result.getContents());
                query = "select BARCODE, ITEM_NAME, DEPT, USER, SORT FROM BARCODE WHERE BARCODE='" + moveobj_barcode.getText() + "'";
                dbinfor.query(query);
                commtask.execute("select_barcode", POST, callback);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }
}
