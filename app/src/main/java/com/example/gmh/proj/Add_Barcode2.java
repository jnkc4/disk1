package com.example.gmh.proj;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.text.DecimalFormat;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;

import static kr.jnkc.tools.Define.POST;

public class Add_Barcode2 extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    int hit = 0;
    String query;
    String work;
    String result;
    String price;
    ArrayList sort_al = new ArrayList();
    DecimalFormat decimalformat = new DecimalFormat("#,###");
    Toolbar toolbar;
    TextView addcode_name_value;
    TextView addcode_count_value;
    TextView addcode_remark;
    EditText addcode_price;
    RadioGroup addcode_radiogroup;
    RadioButton addcode_each;
    RadioButton addcode_group;
    String barcode_type = "each";
    Spinner object_sort_value;
    Intent intent = new Intent();
    CommTask commtask = new CommTask();
    DBInfor dbinfor = new DBInfor();
    final CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object object) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("select_sort")) {
                    sort_al.addAll(data);
                    ArrayAdapter adapter = new ArrayAdapter(Add_Barcode2.this, android.R.layout.simple_spinner_item, sort_al.get("name"));
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    object_sort_value.setAdapter(adapter);
                } else if (ServiceName.equals("wo_no_select")) {
                    if (data.get(0, 0) == null) { // 기존 관리번호가 없을 경우 관리번호를 1로 자동 입력
                        query = "INSERT INTO work(WORK_NO, NAME, SORT, Count, PRICE, REG_DTTM,REMARK) Values('WO2018-1','" + addcode_name_value.getText() + "','" + object_sort_value.getSelectedItem() + "','"
                                + Integer.parseInt(addcode_count_value.getText().toString()) + "'," + Integer.parseInt(price) + ",(select TIMESTAMP(NOW())),'" + addcode_remark.getText() + "')";
                        dbinfor.query(query);
                        commtask.execute("insert_work", POST, callback);
                    } else {
                        query = "INSERT INTO work(WORK_NO, NAME, SORT, Count, PRICE, REG_DTTM,REMARK) Values('WO2018-" + data.get(0, 0).toString() + "','" + addcode_name_value.getText() + "','" + object_sort_value.getSelectedItem() + "','"
                                + Integer.parseInt(addcode_count_value.getText().toString()) + "'," + Integer.parseInt(price) + " ,(select TIMESTAMP(NOW())),'" + addcode_remark.getText() + "')";
                        dbinfor.query(query);
                        commtask.execute("insert_work", POST, callback);
                    }

                } else if (ServiceName.equals("insert_work")) {
                    query = "SELECT * FROM WORK order by WORK_NO DESC limit 1";
                    dbinfor.query(query);
                    commtask.execute("work_no_barcode", POST, callback);
                } else if (ServiceName.equals("work_no_barcode")) {
                    String name = data.get(0, 1).toString();
                    String sort = data.get(0, 2).toString();
                    int count = Integer.parseInt(data.get(0, 3).toString());
                    if (barcode_type.equals("each")) {
                        for (int i = 0; i < count; i++) {
                            int no = i + 1;
                            query = "INSERT INTO BARCODE(WORK_NO, BARCODE, ITEM_NAME, SORT, REG_DTTM, STAT) VALUES('','OE" + work + "-" + no + "','" + name + "','" + sort + "',(select TimeStamp(NOW())), 'I')";
                            dbinfor.query(query);
                            commtask.execute("insert_barcode", POST, callback);
                        }
                    } else {
                        query = "INSERT INTO BARCODE(WORK_NO, BARCODE, ITEM_NAME, SORT, REG_DTTM, STAT) VALUES('" + work + "','OG" + work + "-1','" + name + "','" + sort + "',(select TimeStamp(NOW())), 'I')";
                        dbinfor.query(query);
                        commtask.execute("insert_barcode", POST, callback);
                    }
                } else if (ServiceName.equals("insert_barcode")) {
                    if (hit == 0) {
                        Toast.makeText(Add_Barcode2.this, "관리번호 등록 성공", Toast.LENGTH_SHORT).show();
                        intent = new Intent(Add_Barcode2.this, Result_Barcode.class);
                        intent.putExtra("work_no", work);
                        startActivity(intent);
                        finish();
                        hit++;
                    } else {
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_barcode);
        toolbar = (Toolbar) findViewById(R.id.addcode_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("관리번호 생성");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);

        addcode_name_value = (TextView) findViewById(R.id.addcode_name_text);
        object_sort_value = (Spinner) findViewById(R.id.addcode_sort_value);
        object_sort_value.setOnItemSelectedListener(Add_Barcode2.this);
        addcode_count_value = (TextView) findViewById(R.id.addcode_count_text);
        addcode_price = (EditText) findViewById(R.id.addcode_price_text);
        addcode_remark = (TextView) findViewById(R.id.addcode_remark_text);
        addcode_radiogroup = (RadioGroup) findViewById(R.id.addcode_radiogroup);
        addcode_each = (RadioButton) findViewById(R.id.addcode_each_radio);
        addcode_group = (RadioButton) findViewById(R.id.addcode_group_radio);
        query = "Select * From sort";
        dbinfor.query(query);
        commtask.execute("select_sort", POST, callback);
        RadioButton.OnClickListener radio_check_event = new RadioButton.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (addcode_each.isChecked() == true) {
                    barcode_type = "each";
                } else {
                    barcode_type = "group";
                }
            }
        };
        addcode_each.setChecked(true);
        addcode_each.setOnClickListener(radio_check_event);
        addcode_group.setOnClickListener(radio_check_event);

        addcode_price.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (!s.toString().equals(result) && !s.toString().equals("")) {
                    result = decimalformat.format(Long.parseLong(s.toString().replaceAll(",", "")));
                    addcode_price.setText(result);
                    addcode_price.setSelection(result.length());
                    price = addcode_price.getText().toString();
                    price = price.replaceAll(",", "");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_insert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Add_Barcode2.this);
        switch (item.getItemId()) {
            case android.R.id.home: {
                if (addcode_name_value.getText().toString().equals("") && addcode_count_value.getText().toString().equals("") && addcode_remark.getText().toString().equals("")) {
                    finish();
                    return true;
                } else {
                    builder.setTitle("화면에서 나가시겠습니까?").setMessage("입력된 내용은 저장되지 않습니다.").setPositiveButton("취소", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).setNegativeButton("확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    builder.create();
                    builder.show();
                    return true;
                }
            }
            case R.id.data_insert: {
                if (addcode_name_value.getText().toString().equals("")) {
                    Toast.makeText(this, "비품명을 입력하세요.", Toast.LENGTH_SHORT).show();
                    return true;
                } else if (addcode_count_value.getText().toString().equals("")) {
                    Toast.makeText(this, "수량을 입력하여 주십시오.", Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    query = "select SUBSTRING_INDEX(WORK_NO,'-',-1)+1 from work order by WORK_NO desc limit 1";
                    dbinfor.query(query);
                    commtask.execute("wo_no_select", POST, callback);
                    return true;
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}
