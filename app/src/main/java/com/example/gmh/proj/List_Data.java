package com.example.gmh.proj;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;
import kr.jnkc.core.RecyclerAdapter;

import static kr.jnkc.tools.Define.POST;

public class List_Data extends AppCompatActivity {
    String query;
    String user_dept = null;
    ArrayList item_data = new ArrayList();
    Toolbar toolbar;
    Spinner search_dept;
    TextView dept;
    TextView value;
    Button search;
    RecyclerView recyclerview;
    Intent intent;
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();

    final CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object object) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("select_dept")) {
                    ArrayAdapter adapter = new ArrayAdapter(List_Data.this, android.R.layout.simple_spinner_dropdown_item, data.get("name"));
                    search_dept.setAdapter(adapter);
                    for (int i = 0; i < data.size(); i++) {
                        if (user_dept.equals(data.get(i, 0).toString())) {
                            search_dept.setSelection(i);
                            search.performClick();
                            break;
                        }
                    }
                } else if (ServiceName.equals("select_name")) {
                    dept.setText(search_dept.getSelectedItem() + "의 총 비품 수 : " + data.get(0, 0).toString());
                    query = "Select ITEM_NAME, Count(ITEM_NAME) as COUNT\n" +
                            "From Barcode\n" +
                            "where dept='" + search_dept.getSelectedItem() + "'" +
                            "group by ITEM_NAME";
                    dbinfor.query(query);
                    commtask.execute("select_data", POST, callback);
                } else if (ServiceName.equals("select_data")) {
                    item_data.clear();
                    item_data.addAll(data);
                    if (data.get(0, 0) != null) {
                        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerview.setLayoutManager(layoutManager);
                        recyclerview.setAdapter(recyclerAdapter);
                    } else {
                        recyclerview.setAdapter(null);
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_data);
        toolbar = (Toolbar) findViewById(R.id.listdata_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("비품 통계");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);
        search_dept = (Spinner) findViewById(R.id.listdata_dept_sp);
        dept = (TextView) findViewById(R.id.listdata_dept_text);
        search = (Button) findViewById(R.id.listdata_search_button);
        recyclerview = (RecyclerView) findViewById(R.id.listdata_list_recy);
        intent = getIntent();
        user_dept = intent.getStringExtra("dept");
        query = "Select * from dept";
        dbinfor.query(query);
        commtask.execute("select_dept", POST, callback);

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                query = "Select Count(dept)\n" +
                        "From barcode\n" +
                        "Where dept = '" + search_dept.getSelectedItem().toString() + "'";
                dbinfor.query(query);
                commtask.execute("select_name", POST, callback);
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    // region RecyclerAdapter
    RecyclerAdapter recyclerAdapter = new RecyclerAdapter(item_data) {
        class ViewHolder extends kr.jnkc.core.ViewHolder {
            public TextView item_text;
            public TextView count_text;

            public ViewHolder(View itemview) {
                super(itemview);
                item_text = (TextView) itemview.findViewById(R.id.listdata_name_value);
                count_text = (TextView) itemview.findViewById(R.id.listdata_count_value);
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewtype) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_data_recyclerview, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            String NAME = (String) getItem(position, "ITEM_NAME");
            String COUNT = getItem(position, "COUNT").toString();
            ViewHolder viewholder = (ViewHolder) holder;
            viewholder.item_text.setText(NAME);
            viewholder.count_text.setText(COUNT);
        }
    };
    //endregion
}
