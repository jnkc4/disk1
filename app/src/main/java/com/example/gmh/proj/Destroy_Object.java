package com.example.gmh.proj;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;

import static kr.jnkc.tools.Define.POST;

public class Destroy_Object extends AppCompatActivity {
    String query;
    String remark;
    Toolbar toolbar;
    TextView destroy_barcode;
    TextView destroy_sort;
    TextView destroy_name;
    TextView destroy_dept;
    TextView destroy_user;
    TextView destroy_remark;
    Button destroy_scan;
    IntentIntegrator integrator = new IntentIntegrator(Destroy_Object.this);
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();

    CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object o) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("barcode_search")) {
                    if (data.get(0, 0) == null) {
                        Toast.makeText(Destroy_Object.this, "존재하지 않는 바코드 입니다.", Toast.LENGTH_SHORT).show();
                    } else if (data.get(0, 4).toString().equals("D")) {
                        Toast.makeText(Destroy_Object.this, "이미 폐기된 바코드 입니다.", Toast.LENGTH_SHORT).show();
                    } else {
                        destroy_sort.setText(data.get(0, 0).toString());
                        destroy_name.setText(data.get(0, 1).toString());
                        destroy_dept.setText(data.get(0, 2).toString());
                        destroy_user.setText(data.get(0, 3).toString());
                    }
                } else if (ServiceName.equals("update_barcode")) {
                    if (destroy_remark.getText().toString().equals("")) {
                        remark = destroy_dept.getText() + "부서의 " + destroy_user.getText() + " 님께서 폐기 처분";
                    } else {
                        remark = destroy_remark.getText().toString();
                        query = "INSERT INTO destroy_barcode(BARCODE, SORT, ITEM_NAME, DEPT, USER, REMARK, REG_DTTM, STAT) " +
                                "VALUES('" + destroy_barcode.getText() + "', '" + destroy_sort.getText() + "', '" + destroy_name.getText() + "', '" + destroy_dept.getText() + "', '" + destroy_user.getText() + "' " +
                                ", '" + remark + "', (SELECT TIMESTAMP(NOW())), 'I')";
                        dbinfor.query(query);
                        commtask.execute("insert_log", POST, callback);
                    }
                } else if (ServiceName.equals("insert_log")) {
                    Toast.makeText(Destroy_Object.this, "폐기 처리가 완료되었습니다.", Toast.LENGTH_SHORT).show();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.destroy_object);
        toolbar = (Toolbar) findViewById(R.id.destroy_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("비품 폐기");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);
        destroy_barcode = (TextView) findViewById(R.id.destroy_barcode);
        destroy_name = (TextView) findViewById(R.id.destroy_name);
        destroy_dept = (TextView) findViewById(R.id.destroy_dept);
        destroy_user = (TextView) findViewById(R.id.destroy_user);
        destroy_remark = (TextView) findViewById(R.id.destroy_remark);
        destroy_scan = (Button) findViewById(R.id.destroy_scan_button);
        destroy_sort = (TextView) findViewById(R.id.destroy_sort);
        integrator.initiateScan();
        destroy_scan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                integrator.initiateScan();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_insert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Destroy_Object.this);
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            case R.id.data_insert: {
                builder.setTitle("비품 폐기").setMessage("해당 비품을 정말 폐기하시겠습니까?").setPositiveButton("취소", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                }).setNegativeButton("확인", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        query = "update barcode set STAT='D' where barcode = '" + destroy_barcode.getText() + "'";
                        dbinfor.query(query);
                        commtask.execute("update_barcode", POST, callback);
                    }
                });
                builder.create();
                builder.show();
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == IntentIntegrator.REQUEST_CODE) {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (result.getContents() == null) {
                Toast.makeText(this, "바코드 읽기 취소", Toast.LENGTH_LONG).show();
            } else {
                destroy_barcode.setText(result.getContents());
                query = "SELECT SORT, ITEM_NAME,IFNULL(DEPT,'미배정'),IFNULL(USER,'미배정'), STAT FROM BARCODE WHERE BARCODE='" + destroy_barcode.getText() + "'";
                dbinfor.query(query);
                commtask.execute("barcode_search", POST, callback);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

}
