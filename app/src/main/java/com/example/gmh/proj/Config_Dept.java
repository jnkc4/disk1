package com.example.gmh.proj;

import android.app.AlertDialog;
import android.app.Service;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;

import static kr.jnkc.tools.Define.POST;

public class Config_Dept extends AppCompatActivity {
    // region 선언부
    String query;
    String old_remark;
    TextView dept_name;
    TextView dept_leader;
    TextView dept_remark;
    Toolbar toolbar;
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();
    // endregion
    // region Callback 메서드
    CommTask.Callback configdept_callback = new CommTask.Callback() {

        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object o) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("dept_select")) {
                    dept_name.setText(data.get(0, 0).toString());
                    dept_leader.setText(data.get(0, 1).toString());
                    dept_remark.setText(data.get(0, 2).toString());
                    dept_name.setEnabled(false);
                    old_remark = dept_remark.getText().toString();
                } else if (ServiceName.equals("dept_update")) {
                    Toast.makeText(Config_Dept.this, "정상적으로 수정되었습니다.", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }
    };

    // endregion
    // region Main
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config_dept);
        toolbar = (Toolbar) findViewById(R.id.configdept_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("부서 수정");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);
        dept_name = (TextView) findViewById(R.id.configdept_name_text);
        dept_remark = (TextView) findViewById(R.id.configdept_remark_text);
        dept_leader = (TextView) findViewById(R.id.configdept_leader_text);
        Intent intent = getIntent();
        String name = intent.getExtras().getString("name");
        query = "Select name, IFNULL((select NAME FROM USER where dept = D.name and level=3),'공석') AS leader,remark From DEPT AS D where Name ='" + name + "'";
        dbinfor.query(query);
        commtask.execute("dept_select", POST, configdept_callback);
    }

    // endregion
    // region Toolbar OptionMenu Create & Click Event
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_insert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Config_Dept.this);
        switch (item.getItemId()) {
            case android.R.id.home: {
                if (old_remark.equals(dept_remark.getText().toString())) {
                    finish();
                    return true;
                } else {
                    builder.setTitle("화면에서 나가시겠습니까?").setMessage("입력된 내용은 저장되지 않습니다.").setPositiveButton("취소", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).setNegativeButton("확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    builder.create();
                    builder.show();
                    return true;
                }
            }
            case R.id.data_insert: {
                if (old_remark.equals(dept_remark.getText().toString())) {
                    Toast.makeText(Config_Dept.this, "변동 사항이 없습니다.", Toast.LENGTH_SHORT).show();
                } else {
                    builder.setTitle("부서정보 수정").setMessage("정말 수정하시겠습니까?").setPositiveButton("취소", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    }).setNegativeButton("확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            query = "Update dept Set remark='" + dept_remark.getText() + "' Where name='" + dept_name.getText() + "'";
                            dbinfor.query(query);
                            commtask.execute("dept_update", POST, configdept_callback);
                        }
                    });
                    builder.create();
                    builder.show();
                }
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
    //endregion
}
