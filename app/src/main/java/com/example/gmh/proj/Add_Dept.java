package com.example.gmh.proj;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;

import static kr.jnkc.tools.Define.POST;


public class Add_Dept extends AppCompatActivity {
    // region 선언부
    String query;

    Toolbar toolbar;
    TextView dept_name;
    TextView dept_remark;
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();

    // endregion
    // region Callback 메서드
    final CommTask.Callback deptadd_callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object object) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("wndqhr_dept")) {
                    if ((int) data.get(0, 0) == 0) {
                        query = "Insert into dept(name,remark) values('" + dept_name.getText() + "','" + dept_remark.getText() + "')";
                        dbinfor.query(query);
                        commtask.execute("insert_dept", POST, deptadd_callback);
                    } else {
                        Toast.makeText(Add_Dept.this, "이미 존재하는 부서명입니다.", Toast.LENGTH_SHORT).show();
                    }

                } else if (ServiceName.equals("insert_dept")) {
                    Toast.makeText(Add_Dept.this, "정상 추가 완료", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }
    };

    // endregion
    // region Main
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_dept);
        toolbar = (Toolbar) findViewById(R.id.adddept_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("부서 생성");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);
        dept_name = (TextView) findViewById(R.id.adddept_name_text);
        dept_remark = (TextView) findViewById(R.id.adddept_remark_text);

    }

    // endregion
    // region Toolbar OptionMenu Create & Click Event
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_insert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Add_Dept.this);
        switch (item.getItemId()) {
            case android.R.id.home: {
                if(dept_name.getText().toString().equals("") && dept_remark.getText().toString().equals("")) {
                    finish();
                    return true;
                }else{
                    builder.setTitle("화면에서 나가시겠습니까?").setMessage("입력된 내용은 저장되지 않습니다.").setPositiveButton("취소", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).setNegativeButton("확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    builder.create();
                    builder.show();
                    return true;
                }
            }
            case R.id.data_insert: {
                if (dept_name.getText().toString().equals("")) {
                    Toast.makeText(this, "부서명을 입력하여 주십시오.", Toast.LENGTH_SHORT).show();
                    break;
                } else if (dept_remark.getText().toString().equals("")) {
                    Toast.makeText(this, "부서 정보를 입력하여 주십시오.", Toast.LENGTH_SHORT).show();
                    break;
                }
                query = "select count(*) from dept where name='" + dept_name.getText() + "'";
                dbinfor.query(query);
                commtask.execute("wndqhr_dept", POST, deptadd_callback);
                return true;
            }
        }
        return super.onOptionsItemSelected(item);
    }
    // endregion
}
