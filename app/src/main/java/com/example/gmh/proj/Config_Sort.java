package com.example.gmh.proj;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;

import static kr.jnkc.tools.Define.POST;

public class Config_Sort  extends AppCompatActivity{
    String query;
    String old_remark;
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();
    TextView name;
    TextView remark;
    Toolbar toolbar;

    final CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object object) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if(ServiceName.equals("sort_select")){
                    name.setText(data.get(0,0).toString());
                    name.setEnabled(false);
                    remark.setText(data.get(0,1).toString());
                    old_remark = data.get(0,1).toString();
                }else if(ServiceName.equals("config_sort")){
                    Toast.makeText(Config_Sort.this, "분류가 정상적으로 수정 되었습니다.", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.config_sort);
        toolbar = (Toolbar) findViewById(R.id.configsort_toolbar);
        setSupportActionBar(toolbar);
        android.support.v7.app.ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("분류 수정");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);

        name = (TextView) findViewById(R.id.configsort_name_text);
        remark = (TextView) findViewById(R.id.configsort_remark_text);
        Intent intent = getIntent();
        final String name = intent.getExtras().getString("name");
        query = "Select * From sort where name='"+name+"'";
        dbinfor.query(query);
        commtask.execute("sort_select",POST, callback);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_insert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Config_Sort.this);
        switch (item.getItemId()) {
            case android.R.id.home: {
                if (remark.getText().toString().equals(old_remark)) {
                    finish();
                    return true;
                } else {
                    builder.setTitle("화면에서 나가시겠습니까?").setMessage("입력된 내용은 저장되지 않습니다.").setPositiveButton("취소", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).setNegativeButton("확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    builder.create();
                    builder.show();
                    return true;
                }
            }
            case R.id.data_insert: {
                if(remark.getText().toString().equals(old_remark)){
                    Toast.makeText(this, "변동 사항이 없습니다.", Toast.LENGTH_SHORT).show();
                }else{
                    query = "update sort set remark ='"+ remark.getText() +"' where name ='" + name.getText() + "'";
                    dbinfor.query(query);
                    commtask.execute("config_sort",POST, callback);
                    return true;
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }
}
