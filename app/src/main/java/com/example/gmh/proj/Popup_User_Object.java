package com.example.gmh.proj;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;

import static kr.jnkc.tools.Define.POST;

public class Popup_User_Object extends Activity {
    String query;
    String type;
    Intent intent;
    TextView popup_barcode;
    TextView popup_sort;
    TextView popup_name;
    TextView popup_dept;
    TextView popup_user;
    LinearLayout dept;
    LinearLayout user;
    Button popup_close;
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();

    CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object o) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("select_object")) {
                    popup_barcode.setText(data.get(0,0).toString());
                    popup_sort.setText(data.get(0, 1).toString());
                    popup_name.setText(data.get(0, 2).toString());
                    if (type.equals("main")) {
                        dept.setVisibility(View.GONE);
                        user.setVisibility(View.GONE);
                    } else {
                        popup_dept.setText(data.get(0, 3).toString());
                        popup_user.setText(data.get(0, 4).toString());
                    }
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.popup_user_object);
        popup_barcode = (TextView) findViewById(R.id.objpopup_barcode);
        popup_sort = (TextView) findViewById(R.id.objpopup_sort);
        popup_name = (TextView) findViewById(R.id.objpopup_name);
        popup_dept = (TextView) findViewById(R.id.objpopup_dept);
        popup_user = (TextView) findViewById(R.id.objpopup_user);
        popup_close = (Button) findViewById(R.id.objpopup_close);
        dept = (LinearLayout) findViewById(R.id.objpopup_dept_lay);
        user = (LinearLayout) findViewById(R.id.objpopup_user_lay);
        intent = getIntent();
        String ID = intent.getExtras().getString("BARCODE");
        type = intent.getExtras().getString("TYPE");


        query = "select BARCODE, SORT, ITEM_NAME, IFNULL(DEPT,'미배정'), IFNULL((SELECT NAME FROM USER WHERE ID=B.USER),'미배정') from barcode AS B where barcode = '" + ID + "'";
        dbinfor.query(query);
        commtask.execute("select_object", POST, callback);

        popup_close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
