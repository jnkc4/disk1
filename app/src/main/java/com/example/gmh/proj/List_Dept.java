package com.example.gmh.proj;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;
import kr.jnkc.core.RecyclerAdapter;

import static kr.jnkc.tools.Define.POST;

public class List_Dept extends AppCompatActivity {
    // region 선언부
    String query;
    ArrayList dept_data = new ArrayList();
    Toolbar toolbar;
    RecyclerView recyclerview;
    Intent intent = new Intent();
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();
    // endregion
    // region Create
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_dept);
    }
    // endregion
    // region Callback 메서드
    CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int pid, String ServiceName, ArrayList result, ArrayList data, Object o) {
            int code = (int) result.get(0,0);
            if(code == 200){
                if(ServiceName.equals("select_dept")){
                    dept_data.clear();
                    dept_data.addAll(data);
                    if (data.get(0, 0) != null) {
                        LinearLayoutManager layoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerview.setLayoutManager(layoutManager);
                        recyclerview.setAdapter(recyclerAdapter);
                    }
                }
            }
        }
    };
    // endregion
    // region Main
    @Override
    protected void onResume() {
        super.onResume();
        recyclerview = (RecyclerView) findViewById(R.id.listdept_recy_view);
        toolbar = (Toolbar) findViewById(R.id.dept_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("부서 관리");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);
        query = "Select D.name, IFNULL((select NAME FROM USER where dept = D.name and level=3),'공석') AS leader ,IFNULL((select count(ID) FROM USER WHERE dept = D.name),0) as count " +
                "From dept AS D ";
        dbinfor.query(query);
        commtask.execute("select_dept",POST,callback);

    }
    // endregion
    // region Toolbar OptionMenu Create & Click Event
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home: {
                finish();
                return true;
            }
            case R.id.new_insert: {
                intent = new Intent(List_Dept.this, Add_Dept.class);
                startActivity(intent);
            }
        }
        return super.onOptionsItemSelected(item);
    }
    // endregion
    // region RecyclerAdatper
    RecyclerAdapter recyclerAdapter = new RecyclerAdapter(dept_data) {
        class ViewHolder extends kr.jnkc.core.ViewHolder {
            public TextView dept_name;
            public TextView dept_leader;
            public TextView dept_count;
            public Button dept_config;

            public ViewHolder(View itemview) {
                super(itemview);
                dept_name = (TextView) itemview.findViewById(R.id.listdept_name_text);
                dept_leader = (TextView) itemview.findViewById(R.id.listdept_leader_text);
                dept_count = (TextView) itemview.findViewById(R.id.listdept_count_text);
                dept_config = (Button) itemview.findViewById(R.id.listdept_button);
                dept_config.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        intent = new Intent(List_Dept.this, Config_Dept.class);
                        intent.putExtra("name", dept_name.getText());
                        startActivity(intent);
                    }
                });
            }
        }

        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewtype) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_dept_recyclerview, parent, false);
            ViewHolder viewHolder = new ViewHolder(view);
            return viewHolder;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            String Name = (String) getItem(position, "name");
            String Leader = (String) getItem(position,"leader");
            String Count = getItem(position, "count").toString();
            ViewHolder viewholder = (ViewHolder) holder;
            viewholder.dept_name.setText(Name);
            viewholder.dept_leader.setText(Leader);
            viewholder.dept_count.setText(Count);
        }
    };
    // endregion
}
