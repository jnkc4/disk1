package com.example.gmh.proj;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import kr.jnkc.core.ArrayList;
import kr.jnkc.core.CommTask;
import kr.jnkc.core.DBInfor;

import static kr.jnkc.tools.Define.POST;

public class Add_Object extends AppCompatActivity {
    String id;
    String dept;
    String level;
    String query;
    int count;
    int dept_no;
    int id_no;
    Bundle bundle;
    Toolbar toolbar;
    TextView addobj_barcode;
    TextView addobj_name;
    TextView addobj_sort;
    Spinner addobj_dept;
    TextView addobj_remark;
    Spinner addobj_user;
    Button addobj_scan_button;
    IntentIntegrator integrator = new IntentIntegrator(Add_Object.this);
    Intent intent;
    DBInfor dbinfor = new DBInfor();
    CommTask commtask = new CommTask();

    final CommTask.Callback callback = new CommTask.Callback() {
        @Override
        public void callback(int PID, String ServiceName, ArrayList result, ArrayList data, Object object) {
            int code = (int) result.get(0, 0);
            if (code == 200) {
                if (ServiceName.equals("select_dept")) {
                    ArrayAdapter adapter = new ArrayAdapter(Add_Object.this, android.R.layout.simple_spinner_dropdown_item, data.get("name"));
                    addobj_dept.setAdapter(adapter);
                    if (level.equals("1") || level.equals("2")) {
                        for (int i = 0; i < data.size(); i++) {
                            if (dept.equals(data.get(i, 0))) {
                                dept_no = i;
                                break;
                            }
                        }
                        addobj_dept.setSelection(dept_no);
                        addobj_dept.setEnabled(false);
                    }
                } else if (ServiceName.equals("select_user")) {
                    ArrayAdapter adapter = new ArrayAdapter(Add_Object.this, android.R.layout.simple_spinner_dropdown_item, data.get("ID"));
                    addobj_user.setAdapter(adapter);
                    if (level.equals("1")) {
                        for (int i = 0; i < data.size(); i++) {
                            if (id.equals(data.get(i, 0))) {
                                id_no = i;
                                break;
                            }
                        }
                        addobj_user.setSelection(id_no);
                        addobj_user.setEnabled(false);
                    }
                } else if (ServiceName.equals("barcode_search")) {
                    if (data.get(0, 0) == null) {
                        Toast.makeText(Add_Object.this, "존재하지 않는 바코드 입니다.", Toast.LENGTH_SHORT).show();
                        addobj_sort.setText("");
                        addobj_name.setText("");
                        return;
                    } else if (data.get(0, 3).equals(null) == false) {
                        Toast.makeText(Add_Object.this, "이미 등록된 비품입니다.\n 사용 부서 : " + data.get(0, 2).toString() + "\n 사용자 : " + data.get(0, 3).toString(), Toast.LENGTH_SHORT).show();
                    } else {
                        addobj_name.setText(data.get(0, 1).toString());
                        addobj_sort.setText(data.get(0, 0).toString());
                        addobj_barcode.setEnabled(false);
                        addobj_dept.requestFocus();
                    }
                } else if (ServiceName.equals("update_barcode")) {
                    Toast.makeText(Add_Object.this, "정상 완료", Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_object);
        toolbar = (Toolbar) findViewById(R.id.addobj_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayShowCustomEnabled(true);
        actionbar.setDisplayShowTitleEnabled(true);
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setTitle("비품 등록");
        actionbar.setHomeAsUpIndicator(R.drawable.ic_back);
        addobj_barcode = (TextView) findViewById(R.id.addobj_barcode_text);
        addobj_name = (TextView) findViewById(R.id.addobj_name_text);
        addobj_sort = (TextView) findViewById(R.id.addobj_sort_text);
        addobj_dept = (Spinner) findViewById(R.id.addobj_dept_sp);
        addobj_remark = (TextView) findViewById(R.id.addobj_remark_text);
        addobj_user = (Spinner) findViewById(R.id.addobj_user_sp);
        addobj_scan_button = (Button) findViewById(R.id.addobj_scan_button);
        integrator.initiateScan();
        intent = getIntent();
        bundle = intent.getExtras();
        if (bundle != null) {
            id = bundle.getString("ID");
            dept = bundle.getString("DEPT");
            level = bundle.getString("LEVEL");
        }
        query = "SELECT * FROM dept";
        dbinfor.query(query);
        commtask.execute("select_dept", POST, callback);

        addobj_scan_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 바코드 스캐너 실행
                integrator.initiateScan();
            }
        });

        addobj_dept.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                query = "select * from user where dept ='" + parent.getSelectedItem().toString() + "'";
                dbinfor.query(query);
                commtask.execute("select_user", POST, callback);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        //  com.google.zxing.integration.android.IntentIntegrator.REQUEST_CODE
        //  = 0x0000c0de; // Only use bottom 16 bits
        if (requestCode == IntentIntegrator.REQUEST_CODE) {
            IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
            if (result.getContents() == null) {
                // 취소됨
                Toast.makeText(this, "바코드 읽기 취소", Toast.LENGTH_LONG).show();
            } else {
                // 스캔된 QRCode --> result.getContents()
//                Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
                addobj_barcode.setText(result.getContents());
//                query = "select * FROM BARCODE where barcode='" + result.getContents() + "'";
//                dbinfor.query(query);
//                commtask.execute("barcode_search", POST, callback);
//                query = "SELECT A.WORK_NO, B.NAME, B.SORT, A.USER, A.DEPT FROM BARCODE AS A INNER JOIN WORK AS B ON A.WORK_NO = B.WORK_NO WHERE A.BARCODE='" + addobj_barcode.getText().toString() + "'";
                query = "SELECT SORT, ITEM_NAME, USER, DEPT FROM BARCODE WHERE BARCODE='" + addobj_barcode.getText() + "'";
                dbinfor.query(query);
                commtask.execute("barcode_search", POST, callback);
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.toolbar_insert, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AlertDialog.Builder builder = new AlertDialog.Builder(Add_Object.this);
        switch (item.getItemId()) {
            case android.R.id.home: {
                if (addobj_barcode.getText().toString().equals("") && addobj_remark.getText().toString().equals("")) {
                    finish();
                    return true;
                } else {
                    builder.setTitle("화면에서 나가시겠습니까?").setMessage("입력된 내용은 저장되지 않습니다.").setPositiveButton("취소", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    }).setNegativeButton("확인", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    });
                    builder.create();
                    builder.show();
                    return true;
                }
            }
            case R.id.data_insert: {
                if (addobj_barcode.getText().toString().equals("")) {
                    Toast.makeText(this, "Barcode를 입력하세요", Toast.LENGTH_SHORT).show();
                    return true;
                } else {
                    query = "UPDATE BARCODE SET DEPT= '" + addobj_dept.getSelectedItem().toString() + "', USER='" + addobj_user.getSelectedItem().toString() + "', REMARK='" + addobj_remark.getText().toString() + "'" +
                            ", CON_DTTM = (select TIMESTAMP(NOW()))" +
                            "where Barcode = '" + addobj_barcode.getText().toString() + "'";
                    dbinfor.query(query);
                    commtask.execute("update_barcode", POST, callback);
                    return true;
                }
            }
        }
        return super.onOptionsItemSelected(item);
    }

}
